import React, {useState, useMemo, useCallback} from 'react'
import PropTypes from 'prop-types'

const ViewContext = React.createContext()

const VIEWS = [
  {key: 'infos', name: 'Informations générales'},
  {key: 'masse-eau', name: 'États des masses d’eau'}
]

const BASSIN_VIEWS = [
  {key: 'infos', name: 'Informations générales'},
  {key: 'masse-eau', name: 'États des masses d’eau'},
  {key: 'element-qualite', name: 'Éléments de qualité'}
]

const SESSIONS = [
  'Période en cours',
  '2009-2014',
  '2010-2015',
  '2011-2016'
]

export function ViewContextProvider({bassins, ...props}) {
  const [view, setView] = useState(VIEWS[0])
  const [selectedSession, setSelectedSession] = useState(SESSIONS[0])
  const [isMenuShown, setIsMenuShown] = useState(true)

  const handleSelectView = useCallback(viewKey => {
    const view = VIEWS.find(({key}) => key === viewKey) || BASSIN_VIEWS.find(({key}) => key === viewKey)
    setView(view)
  }, [])

  const value = useMemo(() => ({
    views: VIEWS,
    bassinViews: BASSIN_VIEWS,
    view,
    sessions: SESSIONS,
    selectedSession,
    setSelectedSession,
    isMenuShown,
    setIsMenuShown,
    handleSelectView,
    bassins
  }), [
    view,
    selectedSession,
    setSelectedSession,
    isMenuShown,
    setIsMenuShown,
    handleSelectView,
    bassins
  ])

  return (
    <ViewContext.Provider
      value={value}
      {...props}
    />
  )
}

ViewContextProvider.propTypes = {
  bassins: PropTypes.array.isRequired
}

export const ViewContextConsumer = ViewContext.Consumer

export default ViewContext
