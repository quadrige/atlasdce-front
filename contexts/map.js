import React, {useCallback, useContext, useEffect, useState, useMemo} from 'react'
import PropTypes from 'prop-types'
import maplibre from 'maplibre-gl'
import {flatten} from 'lodash'

import vector from '@/components/map/styles/vector.json'
import {
  massesLineLayer,
  massesTransitionLineLayer,
  bassinsLineLayer,
  bassinsLabels,
  departementsLine,
  departementsLabels,
  communesLineLayer,
  communesLabels
} from '@/components/map/layers'

import LayerContext from './layer'
import ViewContext from './view'
import ElementsQualiteContext from './elements-qualite'

import {bassinsProperties} from '@/lib/bassins-properties'
import {formatLaReunionCode} from '@/lib/format'

const MapContext = React.createContext()

const DEFAULT_CENTER = [1.7, 46.9]
const DEFAULT_ZOOM = 4.4

const defaultColor = '#B3B3B3'
const defaultMasseColor = '#9CBFFF'
const defaultPointColor = '#17a2b8'

const masseEauTypeColors = {
  MEC: '#9d9dff',
  MET: '#1414ff'
}

const environnementColors = {
  2015: '#bbffbb',
  2021: '#00f600',
  2027: '#009400',
  2033: '#004500',
  0: '#7c9122'
}

export const symbols = [
  'circle',
  'cross',
  'square',
  'star',
  'triangle',
  'octagon'
]

export function MapContextProvider({points, reseaux, massesEau, rawPoints, bassinsFeaturesCollection, ...props}) {
  const [map, setMap] = useState(null)
  const [selectedPoint, setSelectedPoint] = useState(null)
  const [selectedBassin, setSelectedBassin] = useState(null)
  const [selectedMasse, setSelectedMasse] = useState(null)
  const [reseauxIdsList, setReseauxIdsList] = useState([])
  const [customReseaux, setCustomReseaux] = useState([])
  const {view, bassins} = useContext(ViewContext)
  const {masseEtat, masseLayerSelected, setMasseLayerSelected} = useContext(LayerContext)
  const {elementsQualiteCtx, setElementsQualiteCtx} = useContext(ElementsQualiteContext)

  const getPointsColors = useCallback(reseaux => {
    const colors = ['match', ['get', 'idWreseau']]
    for (const reseau of reseaux) {
      const {couleur, points} = reseau
      const pointsIds = points.map(({id}) => id)

      for (const pointsId of pointsIds) {
        colors.push(`${pointsId}-${reseau.id}`, couleur)
      }      
    }

    colors.push(defaultPointColor)

    return colors
  }, [])

  const getMassesInfosColors = useCallback(() => {
    if (masseLayerSelected === 'type') {
      const colors = ['match', ['get', 'CdEuMasseD']]

      for (const masse of massesEau) {
        const {type} = masse
        const color = masseEauTypeColors[type]
        const formattedCode = formatLaReunionCode(masse.code)

        colors.push(formattedCode.toUpperCase(), color)
      }

      colors.push(defaultMasseColor)

      return colors
    }

    if (masseLayerSelected === 'environnement') {
      const colors = ['match', ['get', 'CdEuMasseD']]

      for (const masse of massesEau) {
        const {atteinteObjectifDescription} = masse
        const color = atteinteObjectifDescription ? environnementColors[atteinteObjectifDescription.code] : defaultMasseColor
        const formattedCode = formatLaReunionCode(masse.code)

        colors.push(formattedCode.toUpperCase(), color)
      }

      colors.push(defaultMasseColor)

      return colors
    }

    if (masseLayerSelected === 'typologie') {
      const colors = ['match', ['get', 'CdEuMasseD']]

      for (const masse of massesEau) {
        const {description} = masse
        const color = description.color
        const formattedCode = formatLaReunionCode(masse.code)

        colors.push(formattedCode.toUpperCase(), color)
      }

      colors.push(defaultMasseColor)

      return colors
    }

    return defaultMasseColor
  }, [masseLayerSelected, massesEau])

  const getMasseColors = useCallback(() => {
    if (view.key === 'masse-eau' || view.key === 'element-qualite') {
      const colors = ['match', ['get', 'CdEuMasseD']]

      for (const masse of massesEau) {
        const {elementsQualiteEtat} = masse
        let color

        color = masse.etat[masseEtat].couleur

        if (elementsQualiteCtx === 'none' && view.key === 'element-qualite') {
          color = defaultMasseColor
        } else if (elementsQualiteCtx !== 'none') {
          const elementParent = Object.values(elementsQualiteEtat).find(({id}) => id === elementsQualiteCtx)

          color = elementParent && elementParent.etat ? elementParent.etat.couleur : defaultColor
        }

        const formattedCode = formatLaReunionCode(masse.code)

        colors.push(formattedCode.toUpperCase(), color)
      }

      colors.push(defaultColor)

      return colors
    }

    return getMassesInfosColors()
  }, [view, masseEtat, massesEau, elementsQualiteCtx, getMassesInfosColors])

  const getBassinColor = () => {
    const colors = ['match', ['get', 'CdEuBassin']]
    for (const bassin of bassinsProperties) {
      colors.push(bassin.properties.CdEuBassin, defaultColor)
    }

    colors.push('#F8F4F0')

    return colors
  }

  const initMap = useCallback(mapContainer => {
    const map = new maplibre.Map({
      container: mapContainer,
      style: vector,
      center: DEFAULT_CENTER,
      zoom: DEFAULT_ZOOM,
      maxZoom: 19,
      isInteractive: true
    })

    map.addControl(new maplibre.NavigationControl({showCompass: false}))

    map.once('load', () => {
      map.addSource('points', {
        type: 'geojson',
        data: points,
        promoteId: 'idWreseau'
      })

      map.addSource('masses', {
        type: 'vector',
        promoteId: 'gid',
        format: 'pbf',
        tiles: [
          'https://openmaptiles.geo.data.gouv.fr/data/ifremer-atlasdce/{z}/{x}/{y}.pbf'
        ]
      })

      map.addSource('decoupage-administratif', {
        type: 'vector',
        promoteId: 'gid',
        format: 'pbf',
        tiles: [
          'https://openmaptiles.geo.data.gouv.fr/data/decoupage-administratif/{z}/{x}/{y}.pbf'
        ]
      })

      map.addSource('bassins-points', {
        type: 'geojson',
        data: bassinsFeaturesCollection,
        promoteId: 'id'
      })

      const massesFillLayer = {
        id: 'masses-layer',
        type: 'fill',
        source: 'masses',
        'source-layer': 'mdo',
        paint: {
          'fill-color': getMasseColors(),
          'fill-opacity': [
            'case',
            ['boolean', ['feature-state', 'hover'], false],
            0.6,
            0.9
          ]
        }
      }

      const massesTransitionFillLayer = {
        id: 'transition',
        type: 'fill',
        source: 'masses',
        'source-layer': 'mdotransition',
        paint: {
          'fill-color': getMasseColors(),
          'fill-opacity': [
            'case',
            ['boolean', ['feature-state', 'hover'], false],
            0.6,
            0.9
          ]
        }
      }

      const bassinsFillLayer = {
        id: 'bassins-fill',
        type: 'fill',
        source: 'masses',
        'source-layer': 'bassins',
        paint: {
          'fill-color': getBassinColor(),
          'fill-opacity': 0.2
        }
      }

      map.addLayer(bassinsFillLayer)
      map.addLayer(massesFillLayer)
      map.addLayer(massesLineLayer)
      map.addLayer(bassinsLineLayer)
      map.addLayer(departementsLine)
      map.addLayer(bassinsLabels)
      map.addLayer(communesLineLayer)
      map.addLayer(communesLabels)
      map.addLayer(massesTransitionFillLayer)
      map.addLayer(massesTransitionLineLayer)
      map.addLayer(departementsLabels)

      for (const symbol of symbols) {
        map.loadImage(
          `/symbols/${symbol}.png`,
          (error, image) => {
            if (error) {
              throw error
            }

            map.addImage(symbol, image, {sdf: true})
          }
        )

        map.addLayer({
          id: symbol,
          type: 'symbol',
          source: 'points',
          interactive: true,
          layout: {
            'icon-image': symbol,
            'icon-size': 0.25,
            'icon-allow-overlap': true,
            'text-allow-overlap': true
          },
          paint: {
            'icon-color': '#fff',
            'icon-halo-width': 0.8
          }
        })
      }

      setMap(map)
    })
  }, [bassinsFeaturesCollection, getMasseColors, points])

  useEffect(() => {
    if (view.key !== 'infos') {
      setMasseLayerSelected(null)
    }

    if (view.key !== 'element-qualite') {
      setElementsQualiteCtx('none')
    }
  }, [view, setMasseLayerSelected, setElementsQualiteCtx])

  useEffect(() => {
    if (masseEtat && map && map.isStyleLoaded()) {
      map.setPaintProperty('masses-layer', 'fill-color', getMasseColors())
      map.setPaintProperty('transition', 'fill-color', getMasseColors())
    }
  }, [masseEtat, map, getMasseColors])

  const getReseauxList = () => {
    const reseaux = bassins.map(({reseaux}) => {
      const reseau = Object.values(reseaux).map(r => r)
      return flatten(reseau)
    })
    return flatten(reseaux)
  }

  const setPointFilter = useCallback(symbol => {
    map.setLayoutProperty(symbol, 'visibility', 'visible')

    const filter = ['match', ['get', 'idWreseau']]

    if (reseauxIdsList) {
      for (const reseauxId of reseauxIdsList) {
        const reseau = getReseauxList().find(({id}) => id === reseauxId)
        const {points, symbole} = reseau
        const pointsIds = points.map(({id}) => id)
        for (const pointsId of pointsIds) {
          if (symbole === symbol) {
            filter.push(`${pointsId}-${reseau.id}`, true)
          } else if (symbole !== symbol) {
            filter.push(`${pointsId}-${reseau.id}`, false)
          }
        }

      }
      map.setPaintProperty(symbol, 'icon-halo-color', getPointsColors(getReseauxList()))

      filter.push(false)
    }

    return filter
  }, [map, bassins, reseauxIdsList, getPointsColors])

  useEffect(() => {
    if (map) {
      if (reseauxIdsList.length > 0) {
        for (const symbol of symbols) {
          map.setFilter(symbol, setPointFilter(symbol))
        }
      } else {
        for (const symbol of symbols) {
          map.setLayoutProperty(symbol, 'visibility', 'none')
        }
      }
    }
  }, [map, reseauxIdsList, symbols, setPointFilter])

  const value = useMemo(() => ({
    map,
    rawPoints,
    reseaux,
    massesEau,
    initMap,
    selectedPoint,
    setSelectedPoint,
    selectedBassin,
    setSelectedBassin,
    selectedMasse,
    setSelectedMasse,
    reseauxIdsList,
    setReseauxIdsList,
    customReseaux,
    setCustomReseaux
  }), [map,
    rawPoints,
    reseaux,
    massesEau,
    initMap,
    selectedPoint,
    setSelectedPoint,
    selectedBassin,
    setSelectedBassin,
    selectedMasse,
    setSelectedMasse,
    reseauxIdsList,
    setReseauxIdsList,
    customReseaux,
    setCustomReseaux])

  return (
    <MapContext.Provider
      value={value}
      {...props}
    />
  )
}

MapContextProvider.propTypes = {
  points: PropTypes.object.isRequired,
  reseaux: PropTypes.object,
  massesEau: PropTypes.array.isRequired,
  rawPoints: PropTypes.array.isRequired,
  bassinsFeaturesCollection: PropTypes.object.isRequired
}

MapContextProvider.defaultProps = {
  reseaux: null
}

export default MapContext
