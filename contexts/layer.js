import React, {useMemo, useState, useCallback} from 'react'
import PropTypes from 'prop-types'
import {groupBy} from 'lodash'

const ETATS = [
  'global',
  'ecologique',
  'chimique'
]

const LayerContext = React.createContext()

export function LayerContextProvider({massesEau, ...props}) {
  const [masseEtat, setMasseEtat] = useState(ETATS[0])
  const [masseLayerSelected, setMasseLayerSelected] = useState(null)

  const typologies = useCallback(() => {
    const description = []

    for (const masse of massesEau) {
      const {bassinId} = masse
      description.push({bassinId, ...masse.description})
    }

    const groupedByBassin = groupBy(description, 'bassinId')
    const groupedByCode = Object.keys(groupedByBassin).map(key => {
      return {[key]: groupBy(groupedByBassin[key], 'code')}
    })

    return groupedByCode
  }, [massesEau])

  const value = useMemo(() => ({
    masseEtat,
    setMasseEtat,
    masseLayerSelected,
    setMasseLayerSelected,
    typologies: typologies()
  }), [masseEtat, setMasseEtat, masseLayerSelected, setMasseLayerSelected, typologies])

  return (
    <LayerContext.Provider
      value={value}
      {...props}
    />
  )
}

LayerContextProvider.propTypes = {
  massesEau: PropTypes.array.isRequired
}

export const LayerContextConsumer = LayerContext.Consumer

export default LayerContext
