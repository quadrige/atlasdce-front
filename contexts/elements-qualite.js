import React, {useMemo, useState} from 'react'

const ElementsQualiteContext = React.createContext()

export function ElementsQualiteContextProvider(props) {
  const [elementsQualiteCtx, setElementsQualiteCtx] = useState('none')
  const value = useMemo(() => ({
    elementsQualiteCtx,
    setElementsQualiteCtx
  }), [elementsQualiteCtx, setElementsQualiteCtx])

  return (
    <ElementsQualiteContext.Provider
      value={value}
      {...props}
    />
  )
}

export default ElementsQualiteContext
