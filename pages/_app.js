import React from 'react'
import PropTypes from 'prop-types'
import Head from 'next/head'

import 'bootstrap/dist/css/bootstrap.min.css'

import {bassinsToFeatureCollections} from '@/lib/geojson'

import {ViewContextProvider} from '@/contexts/view'
import {MapContextProvider} from '@/contexts/map'
import {ElementsQualiteContextProvider} from '@/contexts/elements-qualite'
import {bassinsProperties} from '@/lib/bassins-properties'
import {LayerContextProvider} from '@/contexts/layer'

function App({Component, pageProps}) {
  let bassinsFeaturesCollection = {}

  if (pageProps.bassins) {
    bassinsFeaturesCollection = bassinsToFeatureCollections(bassinsProperties, pageProps.bassins)
  }

  return (
    <>
      <Head>
        <meta name='viewport' content='width=device-width, initial-scale=1' />
      </Head>

      <LayerContextProvider massesEau={pageProps.massesEau || []}>
        <ViewContextProvider bassins={pageProps.bassins || []} reseaux={pageProps.reseaux || []}>
          <ElementsQualiteContextProvider>
            <MapContextProvider bassinsFeaturesCollection={bassinsFeaturesCollection} rawPoints={pageProps.rawPoints || []} massesEau={pageProps.massesEau || []} points={pageProps.points || {}}>
              <Component {...pageProps} />
            </MapContextProvider>
          </ElementsQualiteContextProvider>
        </ViewContextProvider>
      </LayerContextProvider>

      <style global jsx>{`
        body,
        html,
        #__next {
          display: flex;
          width: 100vw;
          height: 100vh;
          margin: 0;
          padding: 0;
          background-color: #fff;
        }
        .map {
          display: flex;
          height: 100%;
        }
        
        .mapbox-container {
          position: relative;
          width: 100%;
          height: 100%;
        }
        
        .map-container {
          min-width: 100% !important;
        }
        `}</style>
    </>
  )
}

App.propTypes = {
  Component: PropTypes.any.isRequired,
  pageProps: PropTypes.object.isRequired
}

export default App
