import React, {useState, useEffect, useCallback, useContext} from 'react'
import PropTypes from 'prop-types'

import {
  getMasseEau,
  getBassinStats,
  getStats,
  getPoints,
  getBassins,
  getReseaux,
  getMassesEau
} from '@/lib/api'

import MainLayout from 'layouts/main'

import Stats from '@/components/stats'
import ViewContext from '@/contexts/view'
import MassesEau from '@/components/masses-eau'
import ElementQualite from '@/components/element-qualite'
import MapContext from '@/contexts/map'
import ErrorPage from './404'
import {pointsToFeatureCollections} from '@/lib/geojson'
import MassesEauTable from '@/components/bassin/masses-eau-table'
import MasseEauInfos from '@/components/masses-eau/masse-eau-infos'
import ElementQualiteDocuments from '@/components/element-qualite-documents'
import {Container} from 'react-bootstrap'
import ElementsQualiteContext from '@/contexts/elements-qualite'

function Index({stats, bassins, massesEau, codeBassin, codeMasseEau, masseEau, bassin, massesEauList, notFound}) {
  if (notFound) {
    return (
      <ErrorPage />
    )
  }

  const {view} = useContext(ViewContext)
  const {selectedBassin, selectedPoint, setSelectedPoint, selectedMasse, setSelectedMasse, setSelectedBassin} = useContext(MapContext)
  const {elementsQualiteCtx} = useContext(ElementsQualiteContext)
  const [masseInfos, setMasseInfos] = useState(null)
  const [bassinStats, setBassinStats] = useState(null)

  const loadMasse = useCallback(async masseId => {
    const masse = await getMasseEau(masseId)
    setMasseInfos(masse)
  }, [])

  const loadBassinStats = useCallback(async code => {
    const bassinStats = await getBassinStats(code)
    setBassinStats(bassinStats)
  }, [])

  useEffect(() => {
    if (selectedMasse && selectedMasse.id) {
      loadMasse(selectedMasse.id)
    } else {
      setMasseInfos(null)
    }
  }, [loadMasse, selectedMasse])

  useEffect(() => {
    if (bassin) {
      setSelectedBassin(bassin)
    }
  }, [bassin, setSelectedBassin])

  useEffect(() => {
    if (codeBassin) {
      loadBassinStats(codeBassin)
    } else {
      setBassinStats(null)
    }
  }, [selectedBassin, loadBassinStats, setMasseInfos, setBassinStats, codeBassin])

  useEffect(() => {
    if (codeMasseEau && masseEau) {
      setSelectedMasse(masseEau)
    }
  }, [codeMasseEau, masseEau, setSelectedMasse])

  return (
    <MainLayout>
      {view.key === 'infos' && !selectedBassin && <Stats stats={stats} />}
      {view.key === 'infos' && bassinStats && selectedBassin && (
        <>
          <Stats stats={bassinStats} />
          <MassesEauTable title='Liste des masses d’eau' bassin={selectedBassin} codeBassin={selectedBassin.code} massesEauList={massesEauList} />
        </>
      )}
      {view.key === 'masse-eau' &&
        <MassesEau
          selectedPoint={selectedPoint}
          selectedMasse={selectedMasse}
          setSelectedMasse={setSelectedMasse}
          setSelectedPoint={setSelectedPoint}
          bassins={bassins}
          massesEau={massesEau}
          bassin={selectedBassin}
        />}
      {view.key === 'element-qualite' && selectedMasse && masseInfos && elementsQualiteCtx !== 'none' && (
        <>
          <Container>
            <MasseEauInfos selectedMasse={masseInfos} setSelectedMasse={setSelectedMasse} />
          </Container>
          <ElementQualiteDocuments selectedMasse={masseInfos} elementQualite={elementsQualiteCtx} />
        </>
      )}
      {view.key === 'element-qualite' && selectedBassin && <ElementQualite bassin={selectedBassin} />}
      {view.key === 'element-qualite' && !selectedBassin && <Stats stats={stats} />}
    </MainLayout>
  )
}

Index.propTypes = {
  stats: PropTypes.object,
  bassins: PropTypes.array,
  massesEau: PropTypes.array,
  codeBassin: PropTypes.string,
  codeMasseEau: PropTypes.string,
  masseEau: PropTypes.object,
  bassin: PropTypes.object,
  massesEauList: PropTypes.array,
  notFound: PropTypes.bool
}

Index.defaultProps = {
  stats: {},
  bassins: [],
  massesEau: [],
  codeBassin: null,
  codeMasseEau: null,
  masseEau: {},
  bassin: {},
  massesEauList: [],
  notFound: false
}

export async function getServerSideProps({query}) {
  try {
    const stats = await getStats()
    const points = await getPoints()
    const bassins = await getBassins()
    const reseaux = await getReseaux()
    const massesEau = await getMassesEau()
    const requestBassin = bassins.find(({code}) => code === query.codeBassin)
    const massesEauList = massesEau.filter(({bassinId}) => bassinId === requestBassin?.id)
    const masseEau = massesEauList && query?.codeMasseEau ? massesEauList.find(({code}) => code === query.codeMasseEau) : null

    if (requestBassin && query?.codeMasseEau && !masseEau) {
      return {
        props: {
          notFound: true
        }
      }
    }

    if (query?.codeBassin && !requestBassin) {
      return {
        props: {
          notFound: true
        }
      }
    }

    return {
      props: {
        stats,
        points: pointsToFeatureCollections(points),
        rawPoints: points,
        bassins,
        reseaux,
        massesEau,
        codeBassin: query?.codeBassin || null,
        codeMasseEau: query?.codeMasseEau || null,
        bassin: requestBassin || null,
        massesEauList: massesEauList || null,
        masseEau: masseEau || null
      }
    }
  } catch (error) {
    console.log('error', error)
    return {
      props: {
        notFound: true
      }
    }
  }
}

export default Index
