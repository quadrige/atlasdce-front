import React from 'react'
import PropTypes from 'prop-types'
import {Container, Card} from 'react-bootstrap'
import ErrorPage from '../404'
import {getBassins, getBassinStats, getReseauxByBassinId} from '@/lib/api'
import Image from 'next/image'
import logo from '@/public/ifremer_logo.webp'
import BassinsTable from '@/components/bassin/bassins-table'

function FichePage({bassins, stats, reseaux, notFound}) {
  if (notFound) {
    return (
      <ErrorPage />
    )
  }

  return (
    <Container fluid style={{overflowX: 'hidden'}}>
      <Card className='my-2'>
        <Card.Header className='font-weight-bold'>Atlas DCE - Liste des bassins hydrographiques</Card.Header>
        <Card.Body>
          {bassins && stats && reseaux && (
            <BassinsTable bassinsList={bassins} reseaux={reseaux} stats={stats} />
          )}
        </Card.Body>
        <Card.Footer>
          <Image
            src={logo}
            alt='Ifremer logo'
          />
        </Card.Footer>
      </Card>
    </Container>
  )
}

FichePage.propTypes = {
  bassins: PropTypes.array,
  stats: PropTypes.array,
  reseaux: PropTypes.array,
  notFound: PropTypes.bool
}

FichePage.defaultProps = {
  bassins: [],
  stats: [],
  reseaux: []
}

export async function getServerSideProps() {
  try {
    const stats = []
    const reseaux = []
    const bassins = await getBassins()
    await Promise.all(bassins.map(async ({code, id}) => {
      const stat = await getBassinStats(code)
      const rawReseaux = await getReseauxByBassinId(id)
      stats.push({
        code,
        ...stat
      })
      reseaux.push({
        bassinCode: code,
        reseaux: rawReseaux
      })
    }))
    return {
      props: {
        bassins,
        stats,
        reseaux
      }
    }
  } catch (error) {
    console.log('error', error)
    return {
      props: {
        notFound: true
      }
    }
  }
}

export default FichePage
