import React from 'react'
import PropTypes from 'prop-types'
import {Button, Card, Col, Container, Row} from 'react-bootstrap'
import {getBassinByCode, getPoints, getReseauByCode} from '@/lib/api'
import {groupBy} from 'lodash'
import Link from 'next/link'
import ReseauTable from '@/components/reseau/reseau-table'
import Image from 'next/image'
import logo from '@/public/ifremer_logo.webp'
import ErrorPage from '../../../../404'

const typeMasse = {
  MET: 'masses d’eau de transition',
  MEC: 'masses d’eau côtières'
}

function ReseauPage({bassin, reseau, point, notFound}) {
  if (notFound) {
    return (
      <ErrorPage />
    )
  }

  return (
    <Container>
      <Card className='my-2'>
        <Card.Header className='font-weight-bold'> Atlas DCE
          <Link href={{
            pathname: '/fiche/bassin/[bassin]',
            query: {
              bassin: bassin.code
            }
          }}
          >
            {bassin.nom}
          </Link> - Liste des points de surveillance par réseau</Card.Header>
        <Card.Body>
          {Object.keys(point).map(p => (
            <ReseauTable
              key={p}
              bassin={bassin}
              title={`Liste des points de surveillance des ${typeMasse[p]} pour le réseau ${reseau.nom}`}
              points={point[p]}
              reseau={reseau}
            />
          ))}
        </Card.Body>
        <Card.Footer>
          <Row>
            <Col>
              <Image
                src={logo}
                alt='Ifremer logo'
              />
            </Col>
            <Col className='text-right'>
              <Link href={{
                pathname: '/fiche'
              }}
              >
                <Button variant='outline-primary' size='sm'>Retour à la liste des bassins hydrographiques</Button>
              </Link>
            </Col>
          </Row>
        </Card.Footer>
      </Card>
    </Container>
  )
}

ReseauPage.propTypes = {
  bassin: PropTypes.object,
  reseau: PropTypes.object,
  point: PropTypes.object,
  notFound: PropTypes.bool
}

ReseauPage.defaultProps = {
  bassin: {},
  reseau: {},
  point: {},
  notFound: false
}

export async function getServerSideProps({query}) {
  const filterPoints = []

  try {
    const bassin = await getBassinByCode(query.bassin)
    const points = await getPoints()
    const reseau = await getReseauByCode(query.reseau, bassin.id)
    for (const point of points) {
      const {reseaux} = point
      for (const r of reseaux) {
        if (r.code === reseau.code && point.bassinId === bassin.id) {
          filterPoints.push(point)
        }
      }
    }

    const sortByMasseEau = filterPoints.sort((a, b) => {
      return a.masseId - b.masseId
    })
    const groupPointsByMasseType = groupBy(sortByMasseEau, 'masseType')

    return {
      props: {
        bassin,
        reseau,
        point: groupPointsByMasseType
      }
    }
  } catch (error) {
    console.log('error', error)
    return {
      props: {
        notFound: true
      }
    }
  }
}

export default ReseauPage
