import React from 'react'
import PropTypes from 'prop-types'
import {Container, Card, Row, Col, Button} from 'react-bootstrap'
import Image from 'next/image'
import logo from '@/public/ifremer_logo.webp'

import MassesEauTable from '@/components/bassin/masses-eau-table'
import ErrorPage from '../../../404'
import {getBassinByCode, getMassesEauByBassinId} from '@/lib/api'
import Link from 'next/link'

function BassinPage({bassin, massesEauList, notFound}) {
  if (notFound) {
    return (
      <ErrorPage />
    )
  }

  const {code, nom, alert} = bassin
  return (
    <Container fluid style={{overflowX: 'hidden'}}>
      <Card className='my-2'>
        <Card.Header className='font-weight-bold'>Atlas DCE {nom} - Liste des masses d’eau</Card.Header>
        <Card.Body>
          <Card.Title className='text-center'>Bilan provisoire sur les résultats dans le cadre du programme de surveillance de la DCE 2000/60/CE</Card.Title>
          <div>
            <div className='text-justify' dangerouslySetInnerHTML={{__html: alert}} /> {/* eslint-disable-line react/no-danger */}
          </div>
          <MassesEauTable isMapRendering={false} renderType='fiche' bassin={bassin} codeBassin={code} massesEauList={massesEauList} />
        </Card.Body>
        <Card.Footer>
          <Row>
            <Col>
              <Image
                src={logo}
                alt='Ifremer logo'
              />
            </Col>
            <Col className='text-right'>
              <Link href={{
                pathname: '/fiche'
              }}
              >
                <Button variant='outline-primary' size='sm'>Retour à la liste des bassins hydrographiques</Button>
              </Link>
            </Col>
          </Row>
        </Card.Footer>
      </Card>
    </Container>
  )
}

BassinPage.propTypes = {
  bassin: PropTypes.object,
  massesEauList: PropTypes.array,
  notFound: PropTypes.bool
}

BassinPage.defaultProps = {
  bassin: {},
  massesEauList: [],
  notFound: false
}

export async function getServerSideProps({query}) {
  try {
    const bassin = await getBassinByCode(query.bassin)
    const massesEauList = await getMassesEauByBassinId(bassin.id)
    return {
      props: {
        bassin,
        massesEauList
      }
    }
  } catch (error) {
    console.log('error', error)
    return {
      props: {
        notFound: true
      }
    }
  }
}

export default BassinPage
