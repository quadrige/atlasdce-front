import React from 'react'
import PropTypes from 'prop-types'

import {Container} from 'react-bootstrap'
import {getBassinByCode, getMasseEauByCode} from '@/lib/api'
import ErrorPage from '../../../../404'
import MasseCard from '@/components/masses-eau/masse-card'

function MassePage({bassin, masse, notFound}) {
  if (notFound) {
    return (
      <ErrorPage />
    )
  }

  return (
    <Container fluid style={{overflowX: 'hidden'}}>
      {bassin && masse && (
        <MasseCard bassin={bassin} masse={masse} hasReturnButton />
      )}
    </Container>
  )
}

MassePage.propTypes = {
  bassin: PropTypes.object,
  masse: PropTypes.object,
  notFound: PropTypes.bool
}

MassePage.defaultProps = {
  bassin: {},
  masse: {},
  notFound: false
}

export async function getServerSideProps({query}) {
  try {
    const bassin = await getBassinByCode(query.bassin)
    const masse = await getMasseEauByCode(query.masse)

    if (query.bassin !== masse.bassinHydrographique.code) {
      return {
        props: {
          notFound: true
        }
      }
    }

    return {
      props: {
        bassin,
        masse
      }
    }
  } catch (error) {
    console.log('error', error)
    return {
      props: {
        notFound: true
      }
    }
  }
}

export default MassePage
