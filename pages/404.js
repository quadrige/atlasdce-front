import React from 'react'

import Link from 'next/link'

export default function ErrorPage() {
  return (
    <div className='m-auto'>
      <h1>404 - Page introuvable</h1>
      <div className='text-center'>
        <div className='my-4'>
          <Link href='/map'>
            <a>
              Aller vers l’outil cartographique
            </a>
          </Link>
        </div>
        <div>
          <Link href='/fiche'>
            <a>
              Aller vers la liste des bassins
            </a>
          </Link>
        </div>
      </div>
    </div>
  )
}
