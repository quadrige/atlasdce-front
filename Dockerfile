ARG IMAGE_REGISTRY="gitlab-registry.ifremer.fr/ifremer-commons/docker/images/"
ARG DOCKER_IMAGE="node:16.19.0-alpine"

# 1/3 Install dependencies
FROM $IMAGE_REGISTRY$DOCKER_IMAGE AS deps
RUN apk add --no-cache libc6-compat
WORKDIR /atlasdce-front

COPY package.json yarn.lock ./

RUN yarn install --frozen-lockfile

# 2/3 Create build
FROM $IMAGE_REGISTRY$DOCKER_IMAGE AS builder
WORKDIR /atlasdce-front
COPY --from=deps /atlasdce-front/node_modules ./node_modules
COPY . .
RUN NEXT_PUBLIC_ATLAS_API_URL=APP_NEXT_PUBLIC_ATLAS_API_URL NEXT_PUBLIC_DOWNLOAD_PATH=APP_NEXT_PUBLIC_DOWNLOAD_PATH yarn build

# 3/3 Run application
FROM $IMAGE_REGISTRY$DOCKER_IMAGE AS runner
WORKDIR /atlasdce-front

ARG USERNAME="nextjs"
ARG USERID="207506"
ARG GROUPNAME="nodejs"
ARG GROUPID="11679"
ENV NODE_ENV production

RUN \
    echo "Creation group ${GROUPNAME}:${GROUPID}" && \
    addgroup -g ${GROUPID} ${GROUPNAME} && \
    echo "Creatin user ${USERNAME}:${USERID}" && \
    adduser -u ${USERID} -G ${GROUPNAME} -D ${USERNAME}
USER ${USERNAME}:${GROUPNAME}

COPY --chown=${USERNAME}:${GROUPNAME} --from=builder /atlasdce-front/node_modules ./node_modules
COPY --chown=${USERNAME}:${GROUPNAME} --from=builder /atlasdce-front/package.json ./package.json
COPY --chown=${USERNAME}:${GROUPNAME} --from=builder /atlasdce-front/server.js ./server.js
COPY --chown=${USERNAME}:${GROUPNAME} --from=builder /atlasdce-front/entrypoint.sh ./entrypoint.sh

COPY --chown=${USERNAME}:${GROUPNAME} --from=builder /atlasdce-front/public ./public
COPY --chown=${USERNAME}:${GROUPNAME} --from=builder /atlasdce-front/next.config.js ./

COPY --chown=${USERNAME}:${GROUPNAME} --from=builder /atlasdce-front/.next ./.next

RUN ["chmod", "+x", "/atlasdce-front/entrypoint.sh"]
ENTRYPOINT ["/atlasdce-front/entrypoint.sh"]

USER nextjs

EXPOSE 3000

ENV PORT 3000

ENV NEXT_TELEMETRY_DISABLED 1

CMD ["yarn", "start"]
