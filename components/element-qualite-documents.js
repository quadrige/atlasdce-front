import React from 'react'
import PropTypes from 'prop-types'
import DownloadDocuments from './download-documents'
import {flatten} from 'lodash'
import {Col, Container, Row} from 'react-bootstrap'
import ElementQualiteEvaluation from './element-qualite-evalution'
import {format} from 'date-fns'
import {fr} from 'date-fns/locale'
import MasseEauCommentaire from './masses-eau/masse-eau-commentaire'

const DOWNLOAD_PATH = process.env.NEXT_PUBLIC_DOWNLOAD_PATH || 'http://envlit.ifremer.fr/var/envlit/storage/documents/atlas_DCE/upload/doc'

function ElementQualiteDocuments({selectedMasse, elementQualite}) {
  const {code, bassinHydrographique} = selectedMasse
  const children = selectedMasse.elementsQualite.filter(f => f.children).map(({children}) => children)
  const elements = [...selectedMasse.elementsQualite, ...flatten(children)]
  const selectedElement = elements.find(({id}) => id === elementQualite)

  const documents = [
    {
      id: 1,
      exist: Boolean(selectedMasse?.masseDocDesc),
      description: 'Contexte phyisque de la masse d’eau',
      url: `${DOWNLOAD_PATH}/${bassinHydrographique.code}/${selectedMasse?.masseDocDesc}`
    },
    {
      id: 2,
      exist: Boolean(selectedElement?.docMethodo),
      description: 'Calcul de l’indicateur',
      url: `${DOWNLOAD_PATH}/${bassinHydrographique.code}/${selectedElement?.docMethodo}`
    },
    {
      id: 3,
      exist: Boolean(selectedElement?.docProtocole),
      description: 'Protocole d’échantillonage',
      url: `${DOWNLOAD_PATH}/${bassinHydrographique.code}/${selectedElement?.docProtocole}`
    },
    {
      id: 4,
      exist: Boolean(selectedElement?.classementComplementBilan),
      description: `${selectedElement?.nom} : résultat pour le masse d’eau ${code}`,
      url: `${DOWNLOAD_PATH}/${bassinHydrographique.code}/${selectedElement?.classementComplementBilan}`
    }
  ]

  return (
    <>
      <Container className='my-3'>
        <Row noGutters>
          {selectedElement && documents.map(({id, description, url, exist}) => (
            <React.Fragment key={id}>
              {exist && (
                <Col xs={6}>
                  <DownloadDocuments url={url} description={description} />
                </Col>
              )}
            </React.Fragment>
          ))}
        </Row>

        {!selectedElement && documents[0].exist && (
          <DownloadDocuments url={documents[0].url} description={documents[0].description} />
        )}

        {!selectedElement && (
          <MasseEauCommentaire masseEau={selectedMasse} />
        )}
      </Container>
      {selectedElement && (
        <ElementQualiteEvaluation element={selectedElement} />
      )}
      {selectedElement?.classementBilan && (
        <Container fluid className='my-3'>
          <p className='h5'>Explication sur l’évaluation</p>
          <p dangerouslySetInnerHTML={{__html: selectedElement.classementBilan}} /> {/* eslint-disable-line react/no-danger */}
          {selectedElement?.classementDateMaj && (
            <small>Dernière mise à jour : {format(new Date(selectedElement.classementDateMaj.date), 'P', {locale: fr})}</small>
          )}
        </Container>
      )}
      {selectedElement && (
        <MasseEauCommentaire masseEau={selectedMasse} />
      )}
    </>
  )
}

ElementQualiteDocuments.propTypes = {
  selectedMasse: PropTypes.object.isRequired,
  elementQualite: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number
  ]).isRequired
}

export default ElementQualiteDocuments
