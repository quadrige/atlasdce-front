import React from 'react'
import PropTypes from 'prop-types'
import {groupBy} from 'lodash'
import {Col, Row, Tooltip, OverlayTrigger, Badge} from 'react-bootstrap'

const indiceLabels = {
  1: 'faible',
  2: 'moyen',
  3: 'élevé'
}

const getEtat = nom => {
  if (nom.endsWith('écologique')) {
    return 'ecologique'
  }

  if (nom.endsWith(' chimique')) {
    return 'chimique'
  }

  return false
}

const sortEtat = (elementQualite, nom) => {
  const etats = []
  if (getEtat(nom)) {
    return false
  }

  for (const element of elementQualite) {
    if (element.etat) {
      etats.push(element.etat)
    }
  }

  const sortCouleur = etats.sort((a, b) => {
    return b.valeur - a.valeur
  })

  return sortCouleur[0]
}

function MasseEauQualite({isMapRendering, selectedMasse}) {
  const {elementsQualite, etat} = selectedMasse
  const elementsQualiteGroups = groupBy(elementsQualite, 'typeElementQualiteNom')

  return (
    <div className='mt-3'>
      <Row>
        <Col style={{backgroundColor: '#c2c2c2'}} className={isMapRendering ? 'border border-dark' : 'font-weight-bold border border-dark'}>
          État global
        </Col>
        <Col sm={1} style={{backgroundColor: etat.global.couleur}} className={isMapRendering ? 'border border-dark' : 'border border-dark'} />
      </Row>
      <Row className='mt-3'>
        <Col style={{backgroundColor: '#c2c2c2'}} className={isMapRendering ? 'border border-dark' : 'font-weight-bold border border-dark'}>
          État écologique
        </Col>
        <Col sm={1} style={{backgroundColor: etat.ecologique.couleur}} className={isMapRendering ? 'border border-dark' : 'border border-dark'} />
      </Row>
      {elementsQualiteGroups && (
        <div className='mt-3'>
          {Object.keys(elementsQualiteGroups).map(r => (
            <div className={isMapRendering ? '' : 'my-3'} key={r}>
              <Row>
                {r !== 'null' && (
                  <>
                    <Col style={{backgroundColor: '#c2c2c2'}} className={isMapRendering ? 'border border-dark' : 'font-weight-bold border border-dark'}>
                      {r}
                    </Col>
                    <Col sm={1} style={{backgroundColor: getEtat(r) ? etat[getEtat(r)].couleur : (sortEtat(elementsQualiteGroups[r], r)?.couleur || '#c2c2c2')}} className={isMapRendering ? 'border border-dark' : 'border border-dark'} />
                  </>
                )}
              </Row>
              {r !== 'null' && elementsQualiteGroups[r].map(({id, nom, etat, children, indiceConfiance, etatMotif}) => (
                <div key={id}>
                  {}
                  <Row key={id}>
                    <Col style={{backgroundColor: etat && !isMapRendering ? etat.couleur : '', verticalAlign: 'center'}} className='ml-2 border'>
                      {nom}
                    </Col>
                    <Col sm={1} style={{backgroundColor: etat ? etat.couleur : '#c2c2c2', verticalAlign: 'center'}} className='border text-center'>{
                      <OverlayTrigger
                        placement='top'
                        delay={{show: 250, hide: 400}}
                        overlay={
                          <Tooltip id={`tooltip-${id}`}>
                            {etatMotif ? (
                              <div>
                                {etatMotif.libelle}
                              </div>
                            ) : (
                              <>
                                Indice de confiance : <strong>{indiceConfiance} ({indiceLabels[indiceConfiance]})</strong>
                              </>
                            )}
                          </Tooltip>
                        }
                      >
                        <div style={{marginBottom: 4}}>
                          {etatMotif ? (
                            <div style={{marginLeft: '-0.5em'}}>
                              {etatMotif.code}
                            </div>
                          ) : (
                            <div style={{marginLeft: '-0.2em'}}>
                              <Badge variant='dark'>{indiceConfiance}</Badge>
                            </div>
                          )}
                        </div>
                      </OverlayTrigger>
                    }</Col>
                  </Row>
                  {children && children.map(({id, nom, etat, indiceConfiance, etatMotif}) => (
                    <Row key={id}>
                      <Col style={{backgroundColor: etat && !isMapRendering ? etat.couleur : '', verticalAlign: 'center'}} className='ml-4 border'>
                        {nom}
                      </Col>
                      <Col sm={1} style={{backgroundColor: etat ? etat.couleur : '#c2c2c2'}} className='border text-center'>{
                        <OverlayTrigger
                          placement='top'
                          delay={{show: 250, hide: 400}}
                          overlay={
                            <Tooltip id={`tooltip-${id}`}>
                              {etatMotif ? (
                                <div>
                                  {etatMotif.libelle}
                                </div>
                              ) : (
                                <>
                                  Indice de confiance : <strong>{indiceConfiance} ({indiceLabels[indiceConfiance]})</strong>
                                </>
                              )}
                            </Tooltip>
                          }
                        >
                          <div style={{marginBottom: 4}}>
                            {etatMotif ? (
                              <div style={{marginLeft: '-0.5em'}}>
                                {etatMotif.code}
                              </div>
                            ) : (
                              <div style={{marginLeft: '-0.2em'}}>
                                <Badge variant='dark'>{indiceConfiance}</Badge>
                              </div>
                            )}
                          </div>
                        </OverlayTrigger>
                      }</Col>
                    </Row>
                  ))}
                </div>
              ))}
            </div>
          ))}
        </div>
      )}
    </div>
  )
}

MasseEauQualite.propTypes = {
  isMapRendering: PropTypes.bool,
  selectedMasse: PropTypes.object
}

MasseEauQualite.defaultProps = {
  isMapRendering: true,
  selectedMasse: null
}

export default MasseEauQualite
