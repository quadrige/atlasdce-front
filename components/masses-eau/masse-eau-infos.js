import React from 'react'
import PropTypes from 'prop-types'
import {Col, Row, Button, OverlayTrigger, Tooltip} from 'react-bootstrap'
import {groupBy, uniqBy} from 'lodash'

const typeMasse = {
  MET: 'Masse d’eau de transition',
  MEC: 'Masse d’eau côtière'
}

function MasseEauInfos({isMapRendering, hasTitle, selectedMasse, setSelectedMasse}) {
  const {controles} = selectedMasse
  const filteredControle = controles.filter(({groupe}) => groupe === 'Contrôle de surveillance' || groupe === 'Contrôle opérationnel')
  const groupedControles = groupBy(uniqBy(filteredControle, 'code'), 'groupe')

  return (
    <>
      <Row>
        {hasTitle && (
          <Col xl={10}>
            <div className='h6 font-weight-bold'>
              {typeMasse[selectedMasse.type]} {selectedMasse.code} - {selectedMasse.nom}
            </div>
          </Col>
        )}
        {setSelectedMasse && (
          <Col xl={1}>
            <div className='text-right'>
              <Button onClick={() => setSelectedMasse(null)}>X</Button>
            </div>
          </Col>
        )}
      </Row>
      <div>
        <Row>
          <Col className={`text-right ${isMapRendering ? 'small' : 'h4'}`}>
            <span className='font-weight-bold'>Bassin Hydrographique</span>
          </Col>
          <Col className={`${isMapRendering ? 'small' : 'h4'}`}>
            {selectedMasse.bassinHydrographique.nom}
          </Col>
        </Row>
        <Row>
          <Col className={`text-right ${isMapRendering ? 'small' : 'h4'}`}>
            <span className='font-weight-bold'>Département(s)</span>
          </Col>
          <Col className={`${isMapRendering ? 'small' : 'h4'}`}>
            {selectedMasse.departementsNom}
          </Col>
        </Row>
        <Row>
          <Col className={`text-right ${isMapRendering ? 'small' : 'h4'}`}>
            <span className='font-weight-bold'>Type</span>
          </Col>
          <Col className={`${isMapRendering ? 'small' : 'h4'}`}>
            {selectedMasse.description.code} - {selectedMasse.description.libelle}
          </Col>
        </Row>
        <Row>
          <Col className={`text-right ${isMapRendering ? 'small' : 'h4'}`}>
            <span className='font-weight-bold'>Masse d’eau fortement modifiée</span>
          </Col>
          <Col className={`${isMapRendering ? 'small' : 'h4'}`}>
            {selectedMasse.fortModif ? 'oui' : 'non'}
          </Col>
        </Row>
        <Row>
          <Col className={`text-right ${isMapRendering ? 'small' : 'h4'}`}>
            <span className='font-weight-bold'>Objectifs environnementaux</span>
          </Col>
          <Col className={`${isMapRendering ? 'small' : 'h4'}`}>
            {selectedMasse.atteinteObjectif ? selectedMasse.atteinteObjectif.libelle : ''}
          </Col>
        </Row>
        <Row>
          <Col className={`text-right ${isMapRendering ? 'small' : 'h4'}`}>
            <span className='font-weight-bold'>Suivi au titre du programme de suveillance de la DCE 200/60/CE</span>
          </Col>
          <Col className={`${isMapRendering ? 'small' : 'h4'}`}>
            {selectedMasse.suivi ? 'oui' : 'non'}
          </Col>
        </Row>
        <Row>
          <Col className={`text-right ${isMapRendering ? 'small' : 'h4'}`}>
            {groupedControles['Contrôle de surveillance'] ? (
              <OverlayTrigger
                placement={isMapRendering ? 'bottom' : 'top'}
                containerPadding={10}
                overlay={
                  <Tooltip id='tooltip-surveillance'>
                    {groupedControles['Contrôle de surveillance'].map(({code, nom}) => {
                      return (
                        <div key={code}>
                          <p>{code} - {nom}</p>
                        </div>
                      )
                    })}
                  </Tooltip>
                }
              >
                <span className='font-weight-bold'><u>Contrôle de surveillance</u></span>
              </OverlayTrigger>
            ) : (
              <span className='font-weight-bold'>Contrôle de surveillance</span>
            )}
          </Col>
          <Col className={`${isMapRendering ? 'small' : 'h4'}`}>
            {groupedControles['Contrôle de surveillance'] ? 'oui' : 'non'}
          </Col>
        </Row>
        <Row>
          <Col className={`text-right ${isMapRendering ? 'small' : 'h4'}`}>
            {groupedControles['Contrôle opérationnel'] ? (
              <OverlayTrigger
                placement={isMapRendering ? 'bottom' : 'top'}
                containerPadding={10}
                overlay={
                  <Tooltip id='tooltip-operationnel'>
                    {groupedControles['Contrôle opérationnel'].map(({code, nom}) => {
                      return (
                        <div key={code}>
                          <p>{code} - {nom}</p>
                        </div>
                      )
                    })}
                  </Tooltip>
                }
              >
                <span className='font-weight-bold'><u>Contrôle opérationnel</u></span>
              </OverlayTrigger>
            ) : (
              <span className='font-weight-bold'>Contrôle opérationnel</span>
            )}
          </Col>
          <Col className={`${isMapRendering ? 'small' : 'h4'}`}>
            {groupedControles['Contrôle opérationnel'] ? 'oui' : 'non'}
          </Col>
        </Row>
      </div>
    </>
  )
}

MasseEauInfos.propTypes = {
  isMapRendering: PropTypes.bool,
  hasTitle: PropTypes.bool,
  selectedMasse: PropTypes.object.isRequired,
  setSelectedMasse: PropTypes.func
}

MasseEauInfos.defaultProps = {
  isMapRendering: true,
  hasTitle: true,
  setSelectedMasse: null
}

export default MasseEauInfos
