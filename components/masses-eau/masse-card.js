import React from 'react'
import PropTypes from 'prop-types'

import {Button, Card, Col, Container, Row} from 'react-bootstrap'
import Link from 'next/link'
import MasseEauInfos from '@/components/masses-eau/masse-eau-infos'
import MasseEauQualite from '@/components/masses-eau/masse-eau-qualite'
import Image from 'next/image'
import logo from '@/public/ifremer_logo.webp'

const typeMasse = {
  MET: 'Masse d’eau de transition',
  MEC: 'Masse d’eau côtière'
}

function MasseCard({bassin, masse, hasReturnButton}) {
  return (
    <Card className='my-2'>
      <Card.Header className='font-weight-bold'> Atlas DCE&nbsp;
        <Link href={{
          pathname: '/fiche/bassin/[bassin]',
          query: {
            bassin: bassin.code
          }
        }}
        >
          {bassin.nom}
        </Link> - Bilan des résultats par masse d’eau
      </Card.Header>
      {masse && (
        <Card.Body>
          <Card.Title className='text-center font-weight-bold'>{typeMasse[masse.type]} {masse.code} - {masse.nom}</Card.Title>
          <Row>
            <Col className='border rounded-sm mr-1'>
              <Row className='p-3'>
                <Card bg='light' body>
                  <div className='text-justify' dangerouslySetInnerHTML={{__html: bassin.alert}} /> {/* eslint-disable-line react/no-danger */}
                </Card>
              </Row>
              <Row>
                <Container>
                  <MasseEauInfos isMapRendering={false} hasTitle={false} selectedMasse={masse} />
                </Container>
              </Row>
            </Col>
            <Col className='border rounded-sm px-4'>
              <MasseEauQualite isMapRendering selectedMasse={masse} />
            </Col>
          </Row>
        </Card.Body>
      )}
      <Card.Footer>
        <Row>
          <Col>
            <Image
              src={logo}
              alt='Ifremer logo'
            />
          </Col>
          {hasReturnButton && (
            <Col className='text-right'>
              <Link href={{
                pathname: '/fiche/bassin/[bassin]',
                query: {
                  bassin: bassin.code
                }
              }}
              >
                <Button variant='outline-primary' size='sm'>Retour à la liste des masses d’eau</Button>
              </Link>
            </Col>
          )}
        </Row>
      </Card.Footer>
    </Card>
  )
}

MasseCard.propTypes = {
  bassin: PropTypes.object.isRequired,
  masse: PropTypes.object,
  hasReturnButton: PropTypes.bool
}

MasseCard.defaultProps = {
  hasReturnButton: false,
  masse: {}
}

export default MasseCard
