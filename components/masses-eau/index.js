import React, {useCallback, useContext, useEffect, useState} from 'react'
import PropTypes from 'prop-types'
import {flattenDeep, countBy} from 'lodash'
import {Button, Col, Container, Row, Tab, Tabs} from 'react-bootstrap'
import {getMasseEau, getReseauPointParam} from '@/lib/api'

import DownloadDocuments from '../download-documents'
import Counter from '../stats/counter'
import PointsParametresTable from '../points-parametres-table'
import MasseEauInfos from './masse-eau-infos'
import MasseEauQualite from './masse-eau-qualite'
import LayerContext from '@/contexts/layer'
import MassesEtatsButtons from './masses-etats-buttons'
import MasseEauCommentaire from './masse-eau-commentaire'
import PointInfos from '@/components/point/point-infos'

const DOWNLOAD_PATH = process.env.NEXT_PUBLIC_DOWNLOAD_PATH || 'http://envlit.ifremer.fr/var/envlit/storage/documents/atlas_DCE/upload/doc'

function MassesEau({massesEau, bassin, selectedPoint, setSelectedPoint, selectedMasse, setSelectedMasse}) {
  const [key, setKey] = useState()
  const {masseEtat, setMasseEtat} = useContext(LayerContext)
  const [reseauList, setReseauList] = useState()
  const [reseauPointParam, setReseauPointParam] = useState(null)
  const [masseInfos, setMasseInfos] = useState(null)
  const etatsChimique = flattenDeep(massesEau.map(({etat}) => etat.chimique))
  const etatsEcologique = flattenDeep(massesEau.map(({etat}) => etat.ecologique))
  const etatsGlobal = flattenDeep(massesEau.map(({etat}) => etat.global))
  const countChimique = countBy(etatsChimique, 'libelle')
  const countEcologique = countBy(etatsEcologique, 'libelle')
  const countGlobal = countBy(etatsGlobal, 'libelle')
  const downloadUrl = `${DOWNLOAD_PATH}/${masseInfos?.bassinHydrographique?.code}/${masseInfos?.masseDocDesc}`

  const handleChange = event => {
    setMasseEtat(event.currentTarget.value)
  }

  useEffect(() => {
    if (bassin) {
      const {reseaux} = bassin
      const flatReseaux = flattenDeep(Object.keys(reseaux).map(r => reseaux[r]))
      const flattenFind = flatReseaux.find(r => {
        if (selectedPoint && selectedPoint.reseaux[0]) {
          return r.id === selectedPoint.reseaux[0].id
        }

        return null
      })
      setReseauList(flattenFind)
      setKey(flattenFind ? flattenFind.id : null)
    } else {
      setReseauList(null)
      setKey(null)
    }
  }, [bassin, selectedPoint])

  const loadParam = useCallback(async pointId => {
    const pointParam = await getReseauPointParam(pointId)
    setReseauPointParam(pointParam)
  }, [])

  const loadMasse = useCallback(async masseId => {
    const masse = await getMasseEau(masseId)
    setMasseInfos(masse)
  }, [])

  useEffect(() => {
    if (selectedMasse && selectedMasse.id) {
      loadMasse(selectedMasse.id)
    } else {
      setMasseInfos(null)
    }
  }, [loadMasse, selectedMasse])

  useEffect(() => {
    if (selectedPoint && selectedPoint.id) {
      loadParam(selectedPoint.id)
    } else {
      setReseauPointParam(null)
    }
  }, [selectedPoint, loadParam])

  return (
    <>
      <Container>
        {selectedPoint && selectedPoint.reseaux && selectedPoint.reseaux.length > 1 && !selectedMasse && reseauPointParam && (
          <Tabs activeKey={key} onSelect={k => setKey(k)}>
            {selectedPoint.reseaux.map(({id, nom, reseauGroupe}) => {
              const params = reseauPointParam.filter(({reseauId}) => reseauId === id)
              return (
                <Tab key={id} eventKey={id} title={nom}>
                  <Row className='my-4'>
                    <Col xl={10}>
                      <div className='h4'>
                        {selectedPoint.code} - {selectedPoint.nom}
                      </div>
                    </Col>
                    <Col xl={1}>
                      <div className='text-right'>
                        <Button onClick={() => setSelectedPoint(null)}>X</Button>
                      </div>
                    </Col>
                  </Row>
                  <PointInfos point={selectedPoint} reseauGroupe={reseauGroupe} />
                  <PointsParametresTable params={params} />
                </Tab>
              )
            })}
          </Tabs>
        )}
        {selectedPoint && selectedPoint.reseaux && selectedPoint.reseaux.length === 1 && !selectedMasse && (
          <>
            <Row className='mb-4'>
              <Col xl={10}>
                {reseauList && (
                  <div className='h4'>
                    Réseau : {reseauList.nom}
                  </div>
                )}
                <div className='h4'>
                  {selectedPoint.code} - {selectedPoint.nom}
                </div>
              </Col>
              <Col xl={2}>
                <div className='text-right'>
                  <Button onClick={() => setSelectedPoint(null)}>X</Button>
                </div>
              </Col>
            </Row>
            <Row>
              <Col>
                <span className='font-weight-bold'>Masse eau</span> : {selectedPoint.masseNom}
              </Col>
              <Col>
                <span className='font-weight-bold'>Type eau</span> : {selectedPoint.masseType}
              </Col>
            </Row>
            <Row>
              <Col>
                <span className='font-weight-bold'>Type de réseaux</span> : {selectedPoint.reseaux[0]?.reseauGroupe}
              </Col>
              <Col>
                <span className='font-weight-bold'>Point</span> : {selectedPoint.code} - {selectedPoint.nom}
              </Col>
            </Row>
            <Row className='mb-4'>
              <Col>
                <span className='font-weight-bold'>Longitude</span> : {selectedPoint.lon}
              </Col>
              <Col>
                <span className='font-weight-bold'>Latitude</span> : {selectedPoint.lat}
              </Col>
            </Row>
            <PointsParametresTable params={reseauPointParam} />
          </>
        )}
        {(!bassin || !selectedPoint) && !selectedMasse && (
          <>
            <div className='mt-3'>
              <MassesEtatsButtons
                name='masses-etats'
                title='État global'
                value='global'
                onChange={handleChange}
                isChecked={masseEtat === 'global'}
              >
                <Row className='mt-1 mb-5'>
                  <Counter value={countGlobal['Très bon']} label='TRÈS BON ÉTAT' hexaColor='#0089AE' />
                  <Counter value={countGlobal.Bon} label='BON ÉTAT' hexaColor='#50A48B' />
                  <Counter value={countGlobal.Moyen} label='ÉTAT MOYEN' hexaColor='#FBDD6B' />
                  <Counter value={countGlobal['Médiocre']} label='ÉTAT MÉDIOCRE' hexaColor='#EE7313' />
                  <Counter value={countGlobal.Mauvais} label='MAUVAIS ÉTAT' hexaColor='#DA251D' />
                  <Counter value={countGlobal.Inconnu} label='ÉTAT INCONNU' hexaColor='#c2c2c2' />
                </Row>
              </MassesEtatsButtons>
            </div>

            <div className='mt-3 mb-4'>
              <MassesEtatsButtons
                name='masses-etats'
                title='État écologique'
                value='ecologique'
                onChange={handleChange}
                isChecked={masseEtat === 'ecologique'}
              >
                <Row className='mt-1 mb-5'>
                  <Counter value={countEcologique['Très bon']} label='TRÈS BON ÉTAT' hexaColor='#0089AE' />
                  <Counter value={countEcologique.Bon} label='BON ÉTAT' hexaColor='#50A48B' />
                  <Counter value={countEcologique.Moyen} label='ÉTAT MOYEN' hexaColor='#FBDD6B' />
                  <Counter value={countEcologique['Médiocre']} label='ÉTAT MÉDIOCRE' hexaColor='#EE7313' />
                  <Counter value={countEcologique.Mauvais} label='MAUVAIS ÉTAT' hexaColor='#DA251D' />
                  <Counter value={countEcologique.Inconnu} label='ÉTAT INCONNU' hexaColor='#c2c2c2' />
                </Row>

              </MassesEtatsButtons>
            </div>
            <div className='mt-3'>
              <MassesEtatsButtons
                name='masses-etats'
                title='État chimique'
                value='chimique'
                onChange={handleChange}
                isChecked={masseEtat === 'chimique'}
              >
                <Row className='mt-1'>
                  <Counter value={countChimique.Bon} label='BON ÉTAT' color='success' hexaColor='#0089AE' />
                  <Counter value={countChimique.Mauvais} label='MAUVAIS ÉTAT' color='danger' hexaColor='#DA251D' />
                  <Counter value={countChimique.Inconnu} label='INCONNU' color='warning' hexaColor='#B3B3B3' />
                </Row>
              </MassesEtatsButtons>
            </div>
          </>
        )}
        {selectedMasse && !selectedPoint && masseInfos && (
          <>
            <MasseEauInfos selectedMasse={masseInfos} setSelectedMasse={setSelectedMasse} />
            {masseInfos.masseDocDesc && (
              <div className='my-3'>
                <DownloadDocuments url={downloadUrl} description='Contexte phyisque de la masse d’eau' />
              </div>
            )}
            <MasseEauQualite selectedMasse={masseInfos} />
          </>
        )}
      </Container>
      {selectedMasse && !selectedPoint && masseInfos && (
        <MasseEauCommentaire masseEau={masseInfos} />
      )}
    </>
  )
}

MassesEau.propTypes = {
  massesEau: PropTypes.array.isRequired,
  bassin: PropTypes.object,
  selectedPoint: PropTypes.object,
  setSelectedPoint: PropTypes.func.isRequired,
  selectedMasse: PropTypes.object,
  setSelectedMasse: PropTypes.func.isRequired
}

MassesEau.defaultProps = {
  bassin: null,
  selectedPoint: null,
  selectedMasse: null
}

export default MassesEau
