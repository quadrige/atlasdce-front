import React from 'react'
import {ButtonGroup, ToggleButton} from 'react-bootstrap'
import PropTypes from 'prop-types'

function MassesEtatsButtons({name, title, value, onChange, isChecked, children}) {
  return (
    <>
      <ButtonGroup toggle>
        <ToggleButton
          variant='outline-primary'
          size='lg'
          value={value}
          type='radio'
          name={name}
          onChange={onChange}
          checked={isChecked}
        >
          {title}
        </ToggleButton>
      </ButtonGroup>
      {children}
    </>
  )
}

MassesEtatsButtons.propTypes = {
  name: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  value: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  isChecked: PropTypes.bool.isRequired,
  children: PropTypes.element.isRequired
}

export default MassesEtatsButtons
