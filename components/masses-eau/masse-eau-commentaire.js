import React from 'react'
import PropTypes from 'prop-types'
import {Card} from 'react-bootstrap'

function MasseEauCommentaire({masseEau}) {
  return (
    <div>
      {masseEau?.commentaire && (
        <Card border='danger mt-3 p-3 text-justify'>
          <p dangerouslySetInnerHTML={{__html: masseEau?.commentaire}} /> {/* eslint-disable-line react/no-danger */}
        </Card>
      )}
    </div>
  )
}

MasseEauCommentaire.propTypes = {
  masseEau: PropTypes.object.isRequired
}

export default MasseEauCommentaire
