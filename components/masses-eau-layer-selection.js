import LayerContext from '@/contexts/layer'
import React, {useContext} from 'react'
import {ToggleButton, ToggleButtonGroup} from 'react-bootstrap'

const MASSES_LAYERS_INFOS = [
  {
    id: 'type',
    text: 'Masses d’eau Côtières / de transition'
  },
  {
    id: 'environnement',
    text: 'Objectifs environnementaux'
  },
  {
    id: 'typologie',
    text: 'Typologie des masses d’eau'
  }
]

function MassesEauLayerSelection() {
  const {masseLayerSelected, setMasseLayerSelected} = useContext(LayerContext)

  const handleChange = value => {
    setMasseLayerSelected(value)
  }

  const handleReset = event => {
    const {value} = event.target
    if (value && masseLayerSelected === value) {
      setMasseLayerSelected(null)
    }
  }

  return (
    <ToggleButtonGroup
      type='radio'
      name='masse-layer'
      value={masseLayerSelected}
      onClick={handleReset}
      onChange={handleChange}
    >
      {MASSES_LAYERS_INFOS.map(({id, text}) => (
        <ToggleButton variant='outline-primary' key={id} value={id}>
          {text}
        </ToggleButton>
      ))}
    </ToggleButtonGroup>
  )
}

export default MassesEauLayerSelection
