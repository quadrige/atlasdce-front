import MapContext from '@/contexts/map'
import React, {useContext} from 'react'
import {Alert} from 'react-bootstrap'

const DEFAULT_ALERT = 'Les états des ME présentés dans cet atlas et issus des évalutions DCE, ne se substituent en aucun cas aux états des lieux officiels publiés dans les SDAGE de chaque bassin ; ils permettent en revanche de faire le point pendant les 6 ans d’un plan de gestion.'

function MenuFooter() {
  const {selectedBassin} = useContext(MapContext)

  return (
    <Alert variant='danger'>
      {selectedBassin ? (
        <div>
          <small className='text-justify' dangerouslySetInnerHTML={{__html: selectedBassin.alert}} /> {/* eslint-disable-line react/no-danger */}
        </div>
      ) : (
        <small>
          {DEFAULT_ALERT}
        </small>
      )}
    </Alert>
  )
}

export default MenuFooter
