import React from 'react'
import PropTypes from 'prop-types'
import {Table} from 'react-bootstrap'

function PointsParametresTable({params, small}) {
  return (
    <Table responsive striped bordered size={small ? 'sm' : ''}>
      <thead className='align-middle'>
        <tr className='align-middle'>
          <th>Paramètre</th>
          <th>Opérateur terrain</th>
          <th>Opérateur labo</th>
          <th>Dernière année de prélèvement</th>
          <th>Fréquence annuelle</th>
          <th>Période</th>
          <th>Fréquence plan</th>
        </tr>
      </thead>
      <tbody>
        {params && params.map(r => (
          <tr key={r.parametre}>
            <td>{r.parametre}</td>
            <td>{r.operateurTerrain}</td>
            <td>{r.operateurLabo}</td>
            <td className='text-center'>{r.anneePrelevement}</td>
            <td className='text-center'>{r.frequence}</td>
            <td className='text-center'>{r.periode}</td>
            <td className='text-center'>{r.frequencePlan}</td>
          </tr>
        ))}
      </tbody>

      <style jsx>{`
        thead > tr > th, tbody > tr > td {
            vertical-align: middle;
          }

        th, td {
          font-size: ${small ? '70%' : ''};
        }
      `}</style>
    </Table>
  )
}

PointsParametresTable.propTypes = {
  params: PropTypes.array,
  small: PropTypes.bool
}

PointsParametresTable.defaultProps = {
  params: null,
  small: true
}

export default PointsParametresTable
