import React from 'react'
import PropTypes from 'prop-types'
import {Badge, Col, Container, OverlayTrigger, Row, Tooltip} from 'react-bootstrap'
import {format} from 'date-fns'
import {fr} from 'date-fns/locale'

const indiceLabels = {
  1: 'faible',
  2: 'moyen',
  3: 'élevé'
}

function ElementQualiteEvaluation({element}) {
  const {id, nom, etat, etatMotif, indiceConfiance, classementDate} = element

  return (
    <Container>
      <Row>
        <Col className='h5'>{nom}</Col>
      </Row>
      <Row>
        <Col><span className='h5'>Évaluation de la qualité</span> {classementDate && (<small>(en date du {format(new Date(classementDate.date), 'P', {locale: fr})})</small>)}</Col>
        <Col sm={1} style={{backgroundColor: etat ? etat.couleur : '#c2c2c2'}} className='border'>
          <OverlayTrigger
            placement='top'
            delay={{show: 250, hide: 400}}
            overlay={
              <Tooltip id={`tooltip-${id}`}>
                {etatMotif ? (
                  <div>
                    {etatMotif.libelle}
                  </div>
                ) : (
                  <>
                    Indice de confiance : <strong>{indiceConfiance} ({indiceLabels[indiceConfiance]})</strong>
                  </>
                )}
              </Tooltip>
            }
          >
            <div style={{marginBottom: 4}}>
              {etatMotif ? (
                <div style={{marginLeft: '-0.5em'}}>
                  {etatMotif.code}
                </div>
              ) : (
                <div style={{marginLeft: '-0.2em'}}>
                  <Badge variant='dark'>{indiceConfiance}</Badge>
                </div>
              )}
            </div>
          </OverlayTrigger>
        </Col>
      </Row>
    </Container>
  )
}

ElementQualiteEvaluation.propTypes = {
  element: PropTypes.object.isRequired
}

export default ElementQualiteEvaluation
