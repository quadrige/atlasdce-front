import React, {useCallback, useState} from 'react'
import PropTypes from 'prop-types'
import {Table, Spinner} from 'react-bootstrap'
import MasseModal from '@/components/reseau/masse-modal'
import {getMasseEau, getPoint, getReseauPointParam} from '@/lib/api'
import PointModal from '@/components/point/point-modal'
import {uniqBy} from 'lodash'

function ReseauTable({title, bassin, points, reseau}) {
  const [openMasseModal, setOpenMasseModal] = useState(false)
  const [openPointModal, setOpenPointModal] = useState(false)
  const [selectedMasse, setSelectedMasse] = useState(null)
  const [selectedReseauPointParam, setSelectedReseauPointParam] = useState(null)
  const [selectedPoint, setSelectedPoint] = useState(null)
  const [isLoading, setIsLoading] = useState(false)

  const filteredPoints = uniqBy(points, 'id')

  const handleOpenMasseModal = async masseId => {
    setOpenMasseModal(true)
    await loadMasse(masseId)
    setIsLoading(false)
  }

  const handleOpenPointModal = async pointCode => {
    setOpenPointModal(true)
    await loadReseauPointParam(pointCode)
    setIsLoading(false)
  }

  const loadMasse = useCallback(async masseId => {
    setIsLoading(true)
    const masse = await getMasseEau(masseId)
    setSelectedMasse(masse)
  }, [])

  const loadReseauPointParam = useCallback(async pointId => {
    setIsLoading(true)
    const reseauPointParam = await getReseauPointParam(pointId)
    const point = await getPoint(pointId)
    const filteredReseauPointParam = reseauPointParam.filter(({reseauId}) => reseauId === reseau.id)
    setSelectedReseauPointParam(filteredReseauPointParam)
    setSelectedPoint(point)
  }, [reseau])

  return (
    <div className='mt-3'>
      {isLoading && (
        <div className='text-center mb-3'>
          <Spinner style={{width: '3rem', height: '3rem'}} animation='grow' variant='primary' role='status'>
            <span className='sr-only'>Chargement...</span>
          </Spinner>
        </div>
      )}
      {title && (
        <h5 className='text-center'>
          {title}
        </h5>
      )}
      <Table responsive striped bordered size='sm'>
        <thead className='align-middle thead-dark'>
          <tr className='align-middle'>
            <th className='text-center align-middle'>Point</th>
            <th className='text-center align-middle'>Code point</th>
            <th className='text-center align-middle'>Code masse d’eau</th>
            <th className='text-center align-middle'>Nom masse d’eau</th>
            <th className='text-center align-middle'>Longitude</th>
            <th className='text-center align-middle'>Latitude</th>
          </tr>
        </thead>
        <tbody>
          {filteredPoints && filteredPoints.map(p => (
            <tr key={p.id}>
              <td className='text-center font-weight-bold align-middle'>{p.nom}</td>
              <td className='text-center align-middle'><a href='#' onClick={() => handleOpenPointModal(p.id)}>{p.code}</a></td>
              <td className='text-center align-middle'>{p.masseCode}</td>
              <td className='text-center align-middle'><a href='#' onClick={() => handleOpenMasseModal(p.masseId)}>{p.masseNom}</a></td>
              <td className='text-center align-middle'>{p.lon}</td>
              <td className='text-center align-middle'>{p.lat}</td>
            </tr>
          ))}
        </tbody>
      </Table>
      {!isLoading && (
        <MasseModal
          masse={selectedMasse}
          bassin={bassin}
          show={openMasseModal}
          onHide={() => setOpenMasseModal(false)}
        />
      )}
      {!isLoading && (
        <PointModal
          reseau={reseau}
          point={selectedPoint}
          params={selectedReseauPointParam}
          show={openPointModal}
          onHide={() => setOpenPointModal(false)}
        />
      )}
    </div>
  )
}

ReseauTable.propTypes = {
  title: PropTypes.string,
  bassin: PropTypes.object,
  points: PropTypes.array,
  reseau: PropTypes.object
}

ReseauTable.defaultProps = {
  title: null,
  bassin: {},
  points: [],
  reseau: {}
}

export default ReseauTable
