import React from 'react'
import {Modal, Button} from 'react-bootstrap'
import MasseCard from '@/components/masses-eau/masse-card'

function MasseModal(props) {
  return (
    <div>
      <Modal
        {...props}
        aria-labelledby="contained-modal-title-vcenter"
        centered
        contentClassName='masse-modal'
      >
        <Modal.Header closeButton>
        </Modal.Header>
        <Modal.Body>
          <MasseCard bassin={props.bassin} masse={props.masse} />
        </Modal.Body>
        <Modal.Footer>
          <Button onClick={props.onHide}>Fermer</Button>
        </Modal.Footer>
      </Modal>
      <style global jsx>{`
        .modal-dialog {
            width: 90vw !important;
            max-width: 90vw !important;
        } 
      `}</style>
    </div>
  )
}

export default MasseModal
