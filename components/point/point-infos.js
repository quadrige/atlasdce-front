import React from 'react'
import PropTypes from 'prop-types'
import {Col, Row} from 'react-bootstrap'

function PointInfos({point, reseauGroupe}) {
  return (
    <>
      <Row>
        <Col>
          <span className='font-weight-bold'>Masse eau</span> : {point.masseNom}
        </Col>
        <Col>
          <span className='font-weight-bold'>Type eau</span> : {point.masseType}
        </Col>
      </Row>
      <Row>
        <Col>
          <span className='font-weight-bold'>Type de réseaux</span> : {reseauGroupe}
        </Col>
        <Col>
          <span className='font-weight-bold'>Point</span> : {point.code} - {point.nom}
        </Col>
      </Row>
      <Row className='mb-4'>
        <Col>
          <span className='font-weight-bold'>Longitude</span> : {point.lon}
        </Col>
        <Col>
          <span className='font-weight-bold'>Latitude</span> : {point.lat}
        </Col>
      </Row>
    </>
  )
}

PointInfos.propTypes = {
  point: PropTypes.object.isRequired,
  reseauGroupe: PropTypes.string.isRequired
}

export default PointInfos
