import React from 'react'
import PropTypes from 'prop-types'
import {Modal, Button} from 'react-bootstrap'
import PointCard from '@/components/point/point-card'

function PointModal(props) {
  return (
    <div>
      <Modal
        {...props}
        aria-labelledby='contained-modal-title-vcenter'
        centered
        contentClassName='masse-modal'
      >
        <Modal.Header closeButton>
          <Modal.Title>Fiche point réseau</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          {props.point && props.params && props.reseau && (
            <PointCard point={props.point} params={props.params} reseau={props.reseau} />
          )}
        </Modal.Body>
        <Modal.Footer>
          <Button onClick={props.onHide}>Fermer</Button>
        </Modal.Footer>
      </Modal>
    </div>
  )
}

PointModal.propTypes = {
  point: PropTypes.object,
  params: PropTypes.array,
  reseau: PropTypes.object,
  onHide: PropTypes.func.isRequired
}

PointModal.defaultProps = {
  point: {},
  params: [],
  reseau: {}
}

export default PointModal
