import React from 'react'
import PropTypes from 'prop-types'
import {Card} from 'react-bootstrap'
import PointsParametresTable from '@/components/points-parametres-table'
import PointInfos from '@/components/point/point-infos'

function PointCard({point, params, reseau}) {
  const {reseauGroupe} = point.reseaux.find(({code}) => code === reseau.code)

  return (
    <Card className='my-2'>
      <Card.Header className='font-weight-bold'>
        <div>Réseau : {reseau.nom}</div>
        <div>{point.code}</div>
        <div>{point.nom}</div>
      </Card.Header>
      <Card.Body>
        <Card.Title>
          <PointInfos point={point} reseauGroupe={reseauGroupe} />
        </Card.Title>
        <PointsParametresTable small={false} params={params} />
      </Card.Body>
    </Card>
  )
}

PointCard.propTypes = {
  point: PropTypes.object.isRequired,
  params: PropTypes.array.isRequired,
  reseau: PropTypes.object.isRequired
}

export default PointCard
