import React from 'react'
import PropTypes from 'prop-types'

import PictureAsPdfIcon from '@material-ui/icons/PictureAsPdf'

function DownloadDocuments({url, description}) {
  return (
    <div>
      <a target='_blank' href={url} rel='noreferrer'>
        <PictureAsPdfIcon />
        <small>{description}</small>
      </a>
    </div>
  )
}

DownloadDocuments.propTypes = {
  url: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired
}

export default DownloadDocuments
