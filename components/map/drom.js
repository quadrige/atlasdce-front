import React, {useContext, useState} from 'react'
import {format} from 'date-fns'
import {fr} from 'date-fns/locale'
import PropTypes from 'prop-types'
import MapContext from '@/contexts/map'

function Drom({title, fileName, codeBassin, location, setLocation, bassin}) {
  const [isClicked, setIsClicked] = useState(false)
  const {setSelectedPoint, setSelectedBassin} = useContext(MapContext)

  const handleClick = () => {
    if (codeBassin) {
      setSelectedBassin(null)
      setSelectedPoint(null)
      setLocation(codeBassin)
      setIsClicked(true)
    }

    if (isClicked && location === codeBassin) {
      setLocation('FRA')
    }
  }

  return (
    <div className='drom'>
      <div
        className='drom-svg'
        onClick={handleClick}
      >
        <p className='text-center departement'>{isClicked && location === codeBassin ? 'Métropole' : title}</p>
        {bassin && location !== codeBassin && (
          <p className='text-center maj mt-1'>{`(MAJ : ${format(new Date(bassin.dateMaj.date), 'P', {locale: fr})})`}</p>
        )}
        <img src={`/svg/${isClicked && location === codeBassin ? 'france' : fileName}.svg`} />
      </div>
      <style jsx>{`        
        .drom {
          margin-bottom: ${codeBassin && location !== codeBassin ? '11.1em' : '9.7em'};
        }

        .drom-svg {
          position: absolute;
          margin-left: 1em;
          box-shadow: none;
          border: 2px solid #dcd8d5;
          border-radius: 4px;
          z-index: 1;
          padding: 0.5em;
          background-color: ${codeBassin ? 'rgba(255,255,255,0.8)' : 'rgba(192,192,192, 0.8)'};
        }

        .drom-svg:hover {
          cursor: ${codeBassin ? 'pointer' : 'not-allowed'};
          background-color: ${codeBassin ? 'rgba(255,255,255,1)' : 'rgba(192,192,192, 0.8)'}
        }

        .departement {
          line-height: 0.5;
          margin-bottom: ${codeBassin && location !== codeBassin ? '0.5em' : '1em'};
        }

        .maj {
          font-size: x-small;
          font-weight: bold;
          text-align: center;
        }

      `}</style>
    </div>
  )
}

Drom.propTypes = {
  title: PropTypes.string.isRequired,
  fileName: PropTypes.string.isRequired,
  codeBassin: PropTypes.string,
  location: PropTypes.string.isRequired,
  setLocation: PropTypes.func.isRequired,
  bassin: PropTypes.object
}

Drom.defaultProptypes = {
  idBassin: null,
  bassin: null
}

export default Drom
