import React, {useCallback, useContext, useEffect, useMemo, useState} from 'react'
import PropTypes from 'prop-types'
import {useRouter} from 'next/router'
import maplibre from 'maplibre-gl'
import proj4 from 'proj4'
import epsg4471 from 'epsg-index/s/4471.json'

import MapContext, {symbols} from '@/contexts/map'
import LayerContext from '@/contexts/layer'
import ViewContext from '@/contexts/view'

import {
  etatLegend,
  masseEauTypeLegend,
  masseEauEnvironnementLegend,
  masseEauLegend
} from './legends/legends-list'

import SelectReseau from './select-reseau'
import ListReseau from './list-reseau'
import Drom from './drom'
import Legends from './legends'
import {flatten, sortBy} from 'lodash'

import {formatLaReunionCode} from '@/lib/format'
import ElementsQualiteContext from '@/contexts/elements-qualite'

import MenuControl from './menu-control'

const PROJECTION_LIST = {
  'EPSG:27582': '+proj=lcc +lat_1=46.8 +lat_0=46.8 +lon_0=0 +k_0=0.99987742 +x_0=600000 +y_0=2200000 +a=6378249.2 +b=6356515 +towgs84=-168,-60,320,0,0,0,0 +pm=paris +units=m +no_defs',
  'EPSG:2975': '+proj=utm +zone=40 +south +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs',
  'EPSG:4471': epsg4471.proj4
}

const DROM = [
  {
    title: 'La Réunion',
    fileName: 'reunion',
    codeBassin: 'RUN'
  },
  {
    title: 'Mayotte',
    fileName: 'mayotte',
    codeBassin: 'MAY'
  },
  {
    title: 'Guadeloupe',
    fileName: 'guadeloupe'
  },
  {
    title: 'Martinique',
    fileName: 'martinique'
  },
  {
    title: 'Guyane',
    fileName: 'guyane'
  }
]

let hoveredMasseId = null
let hoveredMasseTransitionId = null
let hoveredPointId = null
let hoveredBassinId = null

function AtlasMap({map, data}) {
  const [location, setLocation] = useState('')
  const {
    setSelectedPoint,
    selectedPoint,
    setSelectedMasse,
    selectedBassin,
    massesEau,
    rawPoints,
    setSelectedBassin
  } = useContext(MapContext)
  const {masseLayerSelected, typologies} = useContext(LayerContext)
  const {view, handleSelectView} = useContext(ViewContext)
  const {elementsQualiteCtx} = useContext(ElementsQualiteContext)
  const router = useRouter()

  const typologiesLegend = () => {
    if (selectedBassin) {
      const legend = []
      const typologieLegend = []

      const data = typologies.find(typologie => typologie[selectedBassin.id])

      Object.values(data).map(d => {
        return Object.values(d).map(r => {
          const libelle = `${r[0].code} - ${r[0].libelle}`
          return legend.push({...r[0], libelle})
        })
      })

      const legendSorted = sortBy(legend, o => o.descId)
      const cotieres = legendSorted.filter(l => l.type === 'C')
      const transition = legendSorted.filter(l => l.type === 'T')
      const recif = legendSorted.filter(l => l.type === 'R')

      if (cotieres.length > 0) {
        typologieLegend.push(
          {
            name: 'Masses d’eau cotières',
            values: cotieres
          }
        )
      }

      if (transition.length > 0) {
        typologieLegend.push(
          {
            name: 'Masses d’eau de transition',
            values: transition
          }
        )
      }

      if (recif.length > 0) {
        typologieLegend.push(
          {
            name: 'Masses d’eau récifales',
            values: recif
          }
        )
      }

      return typologieLegend
    }
  }

  const popup = useMemo(() => new maplibre.Popup({
    closeButton: false,
    closeOnClick: false
  }), [])

  const onMasseLeave = useCallback(() => {
    if (hoveredMasseId) {
      highLightMasse(false)
    }

    map.getCanvas().style.cursor = 'grab'
    hoveredMasseId = null
    popup.remove()
  }, [map, highLightMasse, popup])

  const onMasseTransitionLeave = useCallback(() => {
    if (hoveredMasseTransitionId) {
      highLightMasseTransition(false)
    }

    map.getCanvas().style.cursor = 'grab'
    hoveredMasseTransitionId = null
    popup.remove()
  }, [map, highLightMasseTransition, popup])

  const onPointLeave = useCallback(() => {
    if (hoveredPointId) {
      highLightMasse(false)
    }

    map.getCanvas().style.cursor = 'grab'
    hoveredPointId = null
    popup.remove()
  }, [map, highLightMasse, popup])

  const highLightMasse = useCallback(isHovered => {
    map.setFeatureState({source: 'masses', sourceLayer: 'mdo', id: hoveredMasseId}, {hover: isHovered})
  }, [map])

  const highLightMasseTransition = useCallback(isHovered => {
    map.setFeatureState({source: 'masses', sourceLayer: 'mdotransition', id: hoveredMasseTransitionId}, {hover: isHovered})
  }, [map])

  const highLightBassin = useCallback(isHovered => {
    map.setFeatureState({source: 'bassins-points', id: hoveredBassinId}, {hover: isHovered})
  }, [map])

  const onMasseHover = useCallback((e, layer, elementQualite) => {
    let popupText = ''
    if (e.features.length > 0) {
      if (hoveredMasseId) {
        highLightMasse(false)
      }

      hoveredMasseId = e.features[0].id
      highLightMasse(true)
      const {CdEuMasseD, NomMasseDE} = e.features[0].properties
      const masse = massesEau ? massesEau.find(({code}) => formatLaReunionCode(code).toUpperCase() === CdEuMasseD.toUpperCase()) : null

      if (elementQualite && elementQualite !== 'none') {
        const children = masse.elementsQualite.filter(f => f.children).map(({children}) => children)
        const elements = [...masse.elementsQualite, ...flatten(children)]
        const selectedElement = elements.find(({id}) => id === elementQualite)

        if (selectedElement?.nom) {
          popupText = `<h5><strong>${selectedElement.nom}</strong></h5>`
        }
      }

      popupText += `<h5><strong>Masse d'eau côtière</strong> : ${CdEuMasseD} <br /> ${NomMasseDE}</h5>`

      if (layer === 'environnement' && masse.atteinteObjectifDescription) {
        popupText += `<h5><strong>Objectifs environnementaux</strong> : ${masse.atteinteObjectifDescription.libelle}</h5>`
      }

      if (layer === 'typologie' && masse.description) {
        popupText += `<h5><strong>Typologie</strong> : ${masse.description.code} <br /> ${masse.description.libelle}</h5>`
      }

      if (elementQualite && elementQualite !== 'none') {
        const children = masse.elementsQualite.filter(f => f.children).map(({children}) => children)
        const elements = [...masse.elementsQualite, ...flatten(children)]
        const selectedElement = elements.find(({id}) => id === elementQualite)

        if (selectedElement?.statut) {
          popupText += `<h5><strong>${selectedElement.statut.libelle}</strong></h5>`
        }

        if (selectedElement?.indiceConfiance) {
          popupText += `<h5><strong>Indice de confiance</strong> : ${selectedElement.indiceConfiance}</h5>`
        }
      }

      map.getCanvas().style.cursor = 'pointer'
      popup.setLngLat(e.lngLat)
        .setHTML(popupText)
        .addTo(map)
    } else {
      map.getCanvas().style.cursor = 'grab'
      popup.remove()
    }
  }, [map, popup, highLightMasse, massesEau])

  const onMasseTransitionHover = useCallback((e, layer, elementQualite) => {
    if (e.features.length > 0) {
      if (hoveredMasseTransitionId) {
        highLightMasseTransition(false)
      }

      hoveredMasseTransitionId = e.features[0].id
      highLightMasseTransition(true)
      const {CdEuMasseD, NomMasseDE} = e.features[0].properties
      const masse = massesEau ? massesEau.find(({code}) => formatLaReunionCode(code).toUpperCase() === CdEuMasseD.toUpperCase()) : null
      let popupText = `<h5><strong>Masse d'eau de transition</strong> : ${CdEuMasseD} <br /> ${NomMasseDE}</h5>`

      if (layer === 'environnement' && masse.atteinteObjectifDescription) {
        popupText += `<h5><strong>Objectifs environnementaux</strong> : ${masse.atteinteObjectifDescription.libelle}</h5>`
      }

      if (layer === 'typologie' && masse.description) {
        popupText += `<h5><strong>Typologie</strong> : ${masse.description.code} <br /> ${masse.description.libelle}</h5>`
      }

      if (elementQualite) {
        const element = masse?.elementsQualite.find(({id}) => id === elementQualite)
        if (element && element.indiceConfiance) {
          popupText += `<h5><strong>Indice de confiance</strong> : ${element.indiceConfiance}</h5>`
        }
      }

      map.getCanvas().style.cursor = 'pointer'
      popup.setLngLat(e.lngLat)
        .setHTML(popupText)
        .addTo(map)
    } else {
      map.getCanvas().style.cursor = 'grab'
      popup.remove()
    }
  }, [map, popup, highLightMasseTransition, massesEau])

  const onPointHover = useCallback(e => {
    if (e.features.length > 0) {
      if (hoveredPointId) {
        highLightMasse(false)
      }

      hoveredPointId = e.features[0].id
      highLightMasse(true)
      const {code, nom} = e.features[0].properties

      map.getCanvas().style.cursor = 'pointer'
      popup.setLngLat(e.lngLat)
        .setHTML(`<h4><strong>Point</strong> : ${code} <br /> ${nom}</h4>`)
        .addTo(map)
    } else {
      map.getCanvas().style.cursor = 'grab'
      popup.remove()
    }
  }, [map, popup, highLightMasse])

  const onBassinHover = useCallback(e => {
    if (e.features.length > 0) {
      if (hoveredBassinId) {
        highLightBassin(false)
      }

      hoveredBassinId = e.features[0].id
      highLightBassin(true)

      map.getCanvas().style.cursor = 'pointer'
    }
  }, [map, highLightBassin])

  const onBassinLeave = useCallback(() => {
    if (hoveredBassinId) {
      highLightBassin(false)
    }

    map.getCanvas().style.cursor = 'grab'
    hoveredMasseId = null
  }, [map, highLightBassin])

  const handlePointClick = (e, handleSelectView) => {
    if (e.features.length > 0) {
      const pointId = e.features[0].properties.id
      const {bassinId} = e.features[0].properties
      const point = rawPoints ? rawPoints.find(({id}) => id === pointId) : null
      const bassin = data ? data.find(({id}) => id === bassinId) : null
      setSelectedBassin(bassin)
      setSelectedPoint(point)
      setSelectedMasse(null)
      handleSelectView('masse-eau')
    }

    e.originalEvent.cancelBubble = true
  }

  const handleMasseClick = e => {
    if (e.originalEvent.cancelBubble) {
      return
    }

    const masseCode = e.features[0].properties.CdEuMasseD
    const masse = massesEau ? massesEau.find(({code}) => formatLaReunionCode(code).toUpperCase() === masseCode.toUpperCase()) : null

    if (!masse) {
      return
    }

    const bassin = data ? data.find(({id}) => id === masse.bassinId) : null
    router.push(
      `/?map&bassin&codeBassin=${bassin.code}&masse&codeMasseEau=${masse.code}`,
      `/map/bassin/${bassin.code}/masse/${masse.code}`
    )

    setSelectedMasse(masse)
    setSelectedBassin(bassin)
    setSelectedPoint(null)
  }

  const handleBassinClick = e => {
    const bassinId = e.features[0].properties.id
    const bassin = data ? data.find(({id}) => id === bassinId) : null
    setSelectedBassin(bassin)
    router.push(`/?map&bassin&codeBassin=${bassin.code}`, `/map/bassin/${bassin.code}`)
  }

  const handleSelectLocation = code => {
    setLocation(code)

    if (code === 'FRA') {
      router.push('/?map', '/map')
    } else {
      router.push(`/?map&bassin&codeBassin=${code}`, `/map/bassin/${code}`)
    }
  }

  useEffect(() => {
    for (const symbol of symbols) {
      map.on('mousemove', symbol, onPointHover)
      map.on('mouseleave', symbol, onPointLeave)
      map.on('click', symbol, e => handlePointClick(e, handleSelectView))
    }

    map.on('mousemove', 'masses-layer', e => onMasseHover(e, masseLayerSelected, elementsQualiteCtx))
    map.on('mousemove', 'transition', e => onMasseTransitionHover(e, masseLayerSelected, elementsQualiteCtx))
    map.on('mousemove', 'bassins-labels', onBassinHover)

    map.on('mouseleave', 'masses-layer', onMasseLeave)
    map.on('mouseleave', 'transition', onMasseTransitionLeave)
    map.on('mouseleave', 'bassins-labels', onBassinLeave)

    map.on('click', 'masses-layer', e => handleMasseClick(e))
    map.on('click', 'transition', e => handleMasseClick(e))
    map.on('click', 'bassins-labels', e => handleBassinClick(e))

    return () => {
      for (const symbol of symbols) {
        map.off('mousemove', symbol, onPointHover)
        map.off('mouseleave', symbol, onPointLeave)
        map.off('click', symbol, e => handlePointClick(e, handleSelectView))
      }

      map.off('mousemove', 'masses-layer', onMasseHover)
      map.off('mousemove', 'transition', onMasseTransitionHover)
      map.off('mousemove', 'bassins-labels', onBassinHover)

      map.off('mouseleave', 'masses-layer', onMasseLeave)
      map.off('mouseleave', 'transition', onMasseTransitionLeave)
      map.off('mouseleave', 'bassins-labels', onBassinLeave)

      map.off('click', 'masses-layer', e => handleMasseClick(e))
      map.off('click', 'transition', e => handleMasseClick(e))
      map.off('click', 'bassins-labels', e => handleBassinClick(e))
    }
  }, [masseLayerSelected, elementsQualiteCtx, map]) // eslint-disable-line react-hooks/exhaustive-deps

  useEffect(() => {
    if (location === 'FRA' || location === null) {
      setSelectedBassin(null)
    }

    if ((location === 'FRA' || location === null) && view.key === 'element-qualite') {
      handleSelectView('infos')
    }
  }, [location, setSelectedBassin, data, view, handleSelectView])

  useEffect(() => {
    if (location && location === 'FRA') {
      map.fitBounds([-5.317, 41.277, 9.689, 51.234])
    }
  }, [location, map])

  useEffect(() => {
    if (location && location !== 'FRA') {
      const bassin = data.find(({code}) => code === location)
      setSelectedBassin(bassin)
    }
  }, [data, location, setSelectedBassin])

  useEffect(() => {
    if (selectedBassin) {
      setLocation(selectedBassin.code)
      const bassinGeoX = [
        selectedBassin.XMin,
        selectedBassin.YMin
      ]
      const bassinGeoY = [
        selectedBassin.XMax,
        selectedBassin.YMax
      ]

      const projection = PROJECTION_LIST[selectedBassin.bassinProjection]
      const bassinX = proj4(projection).inverse(bassinGeoX)
      const bassinY = proj4(projection).inverse(bassinGeoY)

      map.fitBounds([
        bassinX,
        bassinY
      ])
    }
  }, [selectedBassin, selectedPoint, map])

  return (
    <>
      {view.key !== 'infos' && (
        <SelectReseau
          options={data}
          selected={location || 'FRA'}
          handleSelect={code => handleSelectLocation(code)}
        >
          <ListReseau location={location || 'FRA'} data={data} bassin={selectedBassin} />
        </SelectReseau>
      )}

      {view.key === 'infos' && (
        <SelectReseau
          options={data}
          selected={location || 'FRA'}
          handleSelect={code => handleSelectLocation(code)}
        />
      )}

      {view.key === 'infos' && !masseLayerSelected && (
        <Legends isBassinIsSelected={Boolean(selectedBassin)} legend={masseEauLegend} />
      )}

      {!masseLayerSelected && view.key !== 'infos' && (
        <Legends isBassinIsSelected={Boolean(selectedBassin)} legend={etatLegend} />
      )}

      {masseLayerSelected && masseLayerSelected === 'type' && (
        <Legends isBassinIsSelected={Boolean(selectedBassin)} legend={masseEauTypeLegend} />
      )}

      {masseLayerSelected && masseLayerSelected === 'environnement' && (
        <Legends isBassinIsSelected={Boolean(selectedBassin)} legend={masseEauEnvironnementLegend} />
      )}

      {selectedBassin && masseLayerSelected && masseLayerSelected === 'typologie' && (
        <Legends isBassinIsSelected={Boolean(selectedBassin)} legend={typologiesLegend()} />
      )}

      {!selectedBassin && masseLayerSelected && masseLayerSelected === 'typologie' && (
        <Legends message='Veuillez choisir un bassin' />
      )}

      {!selectedBassin && (
        <div className='drom-container d-flex flex-column'>
          {DROM.map(({title, fileName, codeBassin}) => {
            const bassin = data.find(({code}) => code === codeBassin) || null

            return (
              <div className='drom' key={fileName}>
                <Drom
                  title={title}
                  fileName={fileName}
                  codeBassin={codeBassin}
                  location={location}
                  setLocation={code => handleSelectLocation(code)}
                  bassin={bassin}
                />
              </div>
            )
          })}
        </div>
      )}

      <div className='mapboxgl-ctrl-group mapboxgl-ctrl'>
        <MenuControl />
      </div>
      <style jsx>{`
        .drom-container {
          position: absolute;
          margin-top: 1em;
        }

        .mapboxgl-ctrl-group {
          position: absolute;
          box-shadow: none;
          border: 2px solid #dcd8d5;
          z-index: 2;
          right: 8px;
          top: 80px;
        }
      `}</style>
    </>
  )
}

AtlasMap.propTypes = {
  map: PropTypes.object.isRequired,
  data: PropTypes.array
}

AtlasMap.defaultProps = {
  data: null
}

export default AtlasMap
