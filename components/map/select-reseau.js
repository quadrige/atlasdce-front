import React, {useContext, useState, useCallback, useEffect} from 'react'
import PropTypes from 'prop-types'
import {Form} from 'react-bootstrap'
import MapContext from '@/contexts/map'
import ElementsQualiteContext from '@/contexts/elements-qualite'

function SelectReseau({options, selected, handleSelect, children}) {
  const [isWrap, setIsWrap] = useState(true)
  const [change, setChange] = useState(false)
  const {setSelectedPoint} = useContext(MapContext)
  const {setElementsQualiteCtx} = useContext(ElementsQualiteContext)

  const handleChange = useCallback(value => {
    handleSelect(value)
    setChange(true)
  }, [handleSelect, setChange])

  useEffect(() => {
    if (change) {
      setSelectedPoint(null)
      setElementsQualiteCtx('none')
      setChange(false)
    }
  }, [change, setSelectedPoint, setElementsQualiteCtx])

  return (
    <div
      className='select-paint-container'
      onMouseEnter={() => setIsWrap(false)}
      onMouseLeave={() => setIsWrap(true)}
    >
      <div className='select-wrap'>
        <Form.Control as='select' custom
          value={selected}
          name='paint-layer'
          id='paint-layer'
          onChange={e => handleChange(e.target.value)}
          onClick={() => setIsWrap(false)}
        >
          <option className='france' value='FRA'>France entière</option>
          {options.map(({id, code, nom}) => (
            <option key={id} value={code}>{nom}</option>
          ))}
        </Form.Control>

      </div>

      {!isWrap && children && (
        <div className='content'>
          {children}
        </div>)}

      <style jsx>{`
        .france {
          font-weight: bold;
        }
        .select-paint-container {
          position: absolute;
          box-shadow: none;
          border: 2px solid #dcd8d5;
          border-radius: 4px;
          z-index: 1;
          padding: 0.5em;
          right: 50px;
          top: 9px;
          background-color: rgba(255,255,255,0.9);
        }

        select {
          margin-right: -1em;
          width: 100%;
        }

        .select-wrap {
          display: flex;
          justify-content: space-between;
          align-items: center;
          min-width: 142px;
        }

        .content {
          margin-top: 1em;
        }
      `}
      </style>
    </div>
  )
}

SelectReseau.defaultProps = {
  children: null
}

SelectReseau.propTypes = {
  options: PropTypes.array.isRequired,
  selected: PropTypes.string.isRequired,
  handleSelect: PropTypes.func.isRequired,
  children: PropTypes.node
}

export default SelectReseau
