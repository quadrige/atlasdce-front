export const etatLegend = [
  {
    name: 'État écologique ou global',
    values: [
      {
        color: '#0089AE',
        libelle: 'Très bon'
      },
      {
        color: '#50A48B',
        libelle: 'Bon'
      },
      {
        color: '#FBDD6B',
        libelle: 'Moyen'
      },
      {
        color: '#EE7313',
        libelle: 'Médiocre'
      },
      {
        color: '#DA251D',
        libelle: 'Mauvais'
      },
      {
        color: '#FFFFFF',
        libelle: 'Non pertinent'
      },
      {
        color: '#CC99FF',
        libelle: 'Inférieur au très bon état'
      },
      {
        color: '#c2c2c2',
        libelle: 'Inconnu'
      }
    ]
  },
  {
    name: 'État chimique',
    values: [
      {
        color: '#0089AE',
        libelle: 'Bon'
      },
      {
        color: '#DA251D',
        libelle: 'Mauvais'
      },
      {
        color: '#FFFFFF',
        libelle: 'Non pertinent'
      },
      {
        color: '#c2c2c2',
        libelle: 'Inconnu'
      }
    ]
  },
  {
    name: 'Hydromorphologie',
    values: [
      {
        color: '#0089AE',
        libelle: 'Très bon'
      },
      {
        color: '#CC99FF',
        libelle: 'Inférieur au très bon état'
      },
      {
        color: '#c2c2c2',
        libelle: 'Inconnu'
      }
    ]
  }
]

export const masseEauTypeLegend = [
  {
    name: 'Type de masse d’eau',
    values: [
      {
        color: '#9d9dff',
        libelle: 'Masse d’eau côtière'
      },
      {
        color: '#1414ff',
        libelle: 'Masse d’eau de transition'
      }
    ]
  }
]

export const masseEauEnvironnementLegend = [
  {
    name: 'Objectifs environnementaux',
    values: [
      {
        color: '#bbffbb',
        libelle: 'Atteinte en 2015'
      },
      {
        color: '#00f600',
        libelle: 'Atteinte en 2021'
      },
      {
        color: '#009400',
        libelle: 'Atteinte en 2027'
      },
      {
        color: '#004500',
        libelle: 'Atteinte en 2033'
      },
      {
        color: '#7c9122',
        libelle: 'Risque de non atteinte / objectif moins strict'
      }
    ]
  }
]

export const masseEauLegend = [
  {
    values: [
      {
        color: '#9CBFFF',
        libelle: 'Masse d’eau'
      }
    ]
  }
]
