import React, {useState} from 'react'
import PropTypes from 'prop-types'
import {uniqueId} from 'lodash'

function Legends({isBassinIsSelected, legend, message}) {
  const [isWrap, setIsWrap] = useState(true)

  return (
    <div className='legend'
      onMouseEnter={() => setIsWrap(false)}
      onMouseLeave={() => setIsWrap(true)}
    >
      <div className='legend-title text-center'>Légende</div>

      {!isWrap && legend && legend.map(({name, values}) => (
        <div key={uniqueId()}>
          <div className='font-weight-bold my-2'>{name || ''}</div>
          {values.map(({color, libelle}) => (
            <div className='legend-container' key={libelle}>
              <div className='legend-color' style={{backgroundColor: `${color}`}} />
              <div>{libelle}</div>
            </div>
          ))}
        </div>
      ))}

      {!isWrap && !legend && message && (
        <div className='font-weight-bold my-2'>{message}</div>
      )}

      <style jsx>{`
        .legend {
          position: absolute;
          margin-top: 1em;
          margin-left: ${isBassinIsSelected ? '1em' : '9.5em'};
          box-shadow: none;
          border: 2px solid #dcd8d5;
          border-radius: 4px;
          z-index: 1;
          padding: 0.5em;
          background-color: rgba(255, 255, 255, 0.8);
          max-width: 1000px;
        }

        .legend-title {
          width: 80%;
        }

        .legend-container {
          display: flex;
          align-items: center;
        }

        .legend-color {
          width: 15px;
          height: 15px;
          margin-right: 0.5em;
        }
      `}</style>
    </div>
  )
}

Legends.defaultProps = {
  legend: null,
  message: null,
  isBassinIsSelected: false
}

Legends.propTypes = {
  legend: PropTypes.array,
  message: PropTypes.string,
  isBassinIsSelected: PropTypes.bool
}

export default Legends
