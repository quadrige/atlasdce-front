export const massesLineLayer = {
  id: 'masses-line',
  type: 'line',
  source: 'masses',
  'source-layer': 'mdo',
  paint: {
    'line-color': '#627BC1',
    'line-width': 2
  }
}

export const massesTransitionLineLayer = {
  id: 'masses-transition-line',
  type: 'line',
  source: 'masses',
  'source-layer': 'mdotransition',
  paint: {
    'line-color': '#627BC1',
    'line-width': 2
  }
}

export const bassinsLineLayer = {
  id: 'bassins-line',
  type: 'line',
  source: 'masses',
  'source-layer': 'bassins',
  paint: {
    'line-color': '#D1D3BD',
    'line-width': 1
  },
  minzoom: 3,
  maxzoom: 6
}

export const bassinsLabels = {
  id: 'bassins-labels',
  type: 'symbol',
  source: 'bassins-points',
  minzoom: 3,
  maxzoom: 6,
  layout: {
    'text-field': [
      'format',
      ['get', 'nom'],
      {'font-scale': 1},
      '\n',
      ['get', 'dateMaj'],
      {'font-scale': 0.7}
    ]
  }
}

export const departementsLine = {
  id: 'departements-line',
  type: 'line',
  source: 'decoupage-administratif',
  'source-layer': 'departements',
  paint: {
    'line-color': '#D1D3BD',
    'line-width': 1
  },
  minzoom: 6
}

export const departementsLabels = {
  id: 'departement',
  type: 'symbol',
  source: 'decoupage-administratif',
  'source-layer': 'departements',
  minzoom: 6,
  maxzoom: 8,
  layout: {
    'text-field': ['get', 'nom'],
    'text-padding': 40,
    'text-anchor': 'top',
    'text-offset': [0, 4],
    'text-size': 14,
    'text-allow-overlap': false,
    'text-justify': 'center',
    'icon-allow-overlap': false
  }
}

export const communesLineLayer = {
  id: 'communes-line',
  type: 'line',
  source: 'decoupage-administratif',
  'source-layer': 'communes',
  paint: {
    'line-color': '#D1D3BD',
    'line-width': 1
  }
}

export const communesLabels = {
  id: 'all-communes-labels',
  type: 'symbol',
  source: 'decoupage-administratif',
  'source-layer': 'communes',
  minzoom: 6,
  layout: {
    'text-field': ['get', 'nom'],
    'text-padding': 40,
    'text-anchor': 'top',
    'text-offset': [0, 4],
    'text-size': 14,
    'text-allow-overlap': false,
    'text-justify': 'center',
    'icon-allow-overlap': false
  }
}
