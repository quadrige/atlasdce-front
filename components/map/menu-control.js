import MapContext from '@/contexts/map'
import ViewContext from '@/contexts/view'
import React, {useContext, useEffect} from 'react'
import {Maximize2, Minimize2} from 'react-feather'

function MenuControl() {
  const {map} = useContext(MapContext)
  const {isMenuShown, setIsMenuShown} = useContext(ViewContext)

  useEffect(() => {
    if (!isMenuShown) {
      map.resize()
    }
  }, [map, isMenuShown])

  return (
    <button title={isMenuShown ? 'Étendre la carte' : 'Afficher le menu'} type='button' onClick={() => setIsMenuShown(!isMenuShown)} className='mapboxgl-ctrl'>
      {isMenuShown ? (
        <Maximize2 size={18} />
      ) : (
        <Minimize2 size={18} />
      )}
    </button>
  )
}

export default MenuControl
