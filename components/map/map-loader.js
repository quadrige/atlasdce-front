import React from 'react'
import {Spinner} from 'react-bootstrap'

function MapLoader() {
  return (
    <div className='d-flex flex-column flex-grow-1 justify-content-center align-items-center'>
      <Spinner animation='border' variant='primary' size='xl' />
      <div className='mt-2 text-muted'>Chargement…</div>
    </div>
  )
}

export default MapLoader
