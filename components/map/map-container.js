import React, {useState, useCallback, useEffect, useContext} from 'react'
import PropTypes from 'prop-types'
import Head from 'next/head'
import mapStyle from 'maplibre-gl/dist/maplibre-gl.css'
import {Alert} from 'react-bootstrap'

import MapContext from '@/contexts/map'

function MapContainer({bbox, isLoading, error, children}) {
  const {map, initMap} = useContext(MapContext)

  const [infos, setInfos] = useState(null)
  const [tools, setTools] = useState(null)
  const [mapError, setMapError] = useState(error)

  const mapRef = ref => {
    if (ref && !map) {
      initMap(ref)
    }
  }

  const fitBounds = useCallback(bbox => {
    try {
      map.fitBounds(bbox, {
        padding: 30,
        linear: true,
        maxZoom: 19,
        duration: 0
      })
    } catch {
      setMapError('Aucune position n’est renseignée')
    }
  }, [map])

  useEffect(() => {
    setMapError(error)
  }, [error])

  useEffect(() => {
    if (bbox && map) {
      fitBounds(bbox)
    }
  }, [map, bbox, fitBounds])

  return (
    <div className='mapbox-container'>
      <div className='map'>
        {mapError && (
          <div className='tools error'>
            <Alert variant='danger' message={mapError} />
          </div>
        )}

        {!mapError && isLoading && (
          <div className='tools'>Chargement…</div>
        )}

        {!isLoading && !mapError && infos && (
          <div className='tools'>{infos}</div>
        )}

        {!isLoading && !mapError && tools && (
          <div className='tools right'>{tools}</div>
        )}

        {map && children({
          map,
          setInfos,
          setTools
        })}

        <div ref={mapRef} className='map-container' />
      </div>

      <Head>
        <style key='mapbox'
          dangerouslySetInnerHTML={{__html: mapStyle}} // eslint-disable-line react/no-danger
        />
      </Head>

      <style jsx>{`
          .map-container {
            min-width: 250px;
            flex: 1;
          }

          .tools {
            position: absolute;
            max-height: 100%;
            z-index: 900;
            padding: 0.5em;
            margin: 1em;
            border-radius: 4px;
            background-color: #ffffffbb;
            max-width: 80%;
          }

          .right {
            right: 0;
          }

          .bottom {
            bottom: 0;
            left: 0;
          }

          @media (max-width: 380px) {
            .tools {
              margin: 0.5em;
            }
          }
        `}</style>

    </div>
  )
}

MapContainer.propTypes = {
  bbox: PropTypes.array,
  isLoading: PropTypes.bool,
  error: PropTypes.object,
  children: PropTypes.func.isRequired
}

MapContainer.defaultProps = {
  bbox: null,
  isLoading: false,
  error: null
}

export default MapContainer
