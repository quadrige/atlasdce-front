import React from 'react'
import PropTypes from 'prop-types'
import {Circle, Square, Triangle, Star, X, Octagon} from 'react-feather'

const convertedShapes = {
  circle: Circle,
  square: Square,
  triangle: Triangle,
  star: Star,
  cross: X,
  x: X,
  octagon: Octagon
}

function PointsShapes({shape, color}) {
  const Component = convertedShapes[shape]
  return (
    <Component color={color} />
  )
}

PointsShapes.propTypes = {
  shape: PropTypes.string.isRequired,
  color: PropTypes.string.isRequired
}

export default PointsShapes
