import React, {useContext, useMemo} from 'react'
import PropTypes from 'prop-types'
import {flattenDeep, isEqual} from 'lodash'
import {Form, Tab, Tabs} from 'react-bootstrap'
import MapContext from '@/contexts/map'
import PointsShapes from './points-shapes'

function ListReseau({location, data, bassin}) {
  const {reseauxIdsList, setReseauxIdsList, customReseaux, setCustomReseaux} = useContext(MapContext)
  const france = data.map(({reseaux}) => reseaux)
  const reseauCategory = useMemo(() => [
    {id: 1, title: 'Contrôle de Surveillance', values: flattenDeep(france.map(r => r['Contrôle de surveillance']))},
    {id: 2, title: 'Contrôle opérationnel', values: flattenDeep(france.map(r => r['Contrôle opérationnel']))},
    {id: 3, title: 'Suivi complémentaire de bassin', values: flattenDeep(france.map(r => r['Suivi complémentaire de bassin']))}
  ], [france])

  const handleCategorySelect = id => {
    setCustomReseaux(selectedId => {
      if (selectedId.includes(id)) {
        const filteredReseaux = customReseaux.filter(f => f !== id)
        const filteredCategories = reseauCategory.filter(({id}) => filteredReseaux.includes(id))

        const values = filteredCategories.map(({values}) => values.filter(Boolean))
        const ids = values.map(value => value.map(({id}) => id))
        const allIds = flattenDeep(ids)

        setReseauxIdsList(allIds)

        return filteredReseaux
      }

      const reseaux = [...customReseaux, id]
      const filteredCategories = reseauCategory.filter(({id}) => reseaux.includes(id))

      const values = filteredCategories.map(({values}) => values.filter(Boolean))
      const ids = values.map(value => value.map(({id}) => id))
      const allIds = flattenDeep(ids)

      setReseauxIdsList(allIds)

      return reseaux
    })
  }

  const handleSubcategorySelect = id => {
    setReseauxIdsList(selectedId => {
      if (selectedId.includes(id)) {
        return reseauxIdsList.filter(f => f !== id)
      }

      return [...reseauxIdsList, id]
    })
  }

  return (
    <>
      {location === 'FRA' && !bassin && (
        <>
          {reseauCategory.map(({id, title, values}) => {
            const categoryValues = values.filter(Boolean).map(({id}) => id)

            return (
              <div key={id} className='legend-title'>
                <Form.Check
                  checked={isEqual(reseauxIdsList.sort(), categoryValues.sort()) || customReseaux.includes(id)}
                  custom
                  name={id}
                  onChange={() => handleCategorySelect(id)}
                  type='checkbox'
                  id={id}
                  label={title}
                  value={id}
                />
              </div>
            )
          })}
        </>
      )}
      {bassin && (
        <Tabs defaultActiveKey='first'>
          {bassin.reseaux['Contrôle de surveillance'] && (
            <Tab eventKey='first' title='Contrôle de surveillance'>
              {bassin.reseaux['Contrôle de surveillance'].map(({id, nom, couleur, symbole}) => (
                <div key={id} className='d-flex align-items-center justify-content-between'>
                  <Form.Check
                    key={id}
                    checked={reseauxIdsList.includes(id)}
                    custom
                    onChange={() => handleSubcategorySelect(id)}
                    type='checkbox'
                    id={id}
                    name={id}
                    label={nom}
                    value={id}
                  />
                  {symbole ? (
                    <PointsShapes shape={symbole} color={couleur} />
                  ) : (
                    <div className='color ' style={{backgroundColor: `${couleur}`}} />
                  )}
                </div>
              ))}
            </Tab>
          )}
          {bassin.reseaux['Contrôle opérationnel'] && (
            <Tab eventKey='second' title='Contrôle opérationnel'>
              {bassin.reseaux['Contrôle opérationnel'].map(({id, nom, couleur, symbole}) => (
                <div key={id} className='d-flex align-items-center justify-content-between'>
                  <Form.Check
                    key={id}
                    checked={reseauxIdsList.includes(id)}
                    custom
                    onChange={() => handleSubcategorySelect(id)}
                    type='checkbox'
                    id={id}
                    name={id}
                    label={nom}
                    value={id}
                  />
                  {symbole ? (
                    <PointsShapes shape={symbole} color={couleur} />
                  ) : (
                    <div className='color ' style={{backgroundColor: `${couleur}`}} />
                  )}
                </div>
              ))}
            </Tab>
          )}
          {bassin.reseaux['Suivi complémentaire de bassin'] && (
            <Tab eventKey='third' title='Suivi complémentaire de bassin'>
              {bassin.reseaux['Suivi complémentaire de bassin'].map(({id, nom, couleur, symbole}) => (
                <div key={id} className='d-flex align-items-center justify-content-between'>
                  <Form.Check
                    key={id}
                    checked={reseauxIdsList.includes(id)}
                    custom
                    onChange={() => handleSubcategorySelect(id)}
                    type='checkbox'
                    id={id}
                    name={id}
                    label={nom}
                    value={id}
                  />
                  {symbole ? (
                    <PointsShapes shape={symbole} color={couleur} />
                  ) : (
                    <div className='color ' style={{backgroundColor: `${couleur}`}} />
                  )}
                </div>
              ))}
            </Tab>
          )}
        </Tabs>
      )}
      <style jsx>{`
        .legend-title {
          font-weight: bold;
          width: 80%;
          margin-left: 0.8em;
          margin-bottom: 0.5em;
        }

        .color {
          width: 15px;
          height: 15px;
          border-radius: 50%;
        }
      `}
      </style>
    </>
  )
}

ListReseau.propTypes = {
  location: PropTypes.string.isRequired,
  data: PropTypes.array.isRequired,
  bassin: PropTypes.object
}

ListReseau.defaultProps = {
  bassin: null
}

export default ListReseau
