import React, {useContext} from 'react'
import dynamic from 'next/dynamic'

import MapLoader from '@/components/map/map-loader'
import AtlasMap from '@/components/map/atlas-map'
import ViewContext from '@/contexts/view'

const MapContainer = dynamic(() => import('./map-container'), {
  ssr: false,
  loading: props => (
    <MapLoader {...props} />
  )
})

function MapWrapper() {
  const {bassins} = useContext(ViewContext)

  return (
    <div className='d-flex h-100 w-100'>
      <MapContainer>
        {({map}) => (
          <AtlasMap
            map={map}
            data={bassins}
          />
        )}
      </MapContainer>
    </div>
  )
}

export default MapWrapper
