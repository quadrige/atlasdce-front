import React, {useContext} from 'react'
import PropTypes from 'prop-types'
import {Container} from 'react-bootstrap'

import size from '@/styles/size'

import MenuFooter from '@/components/menu-footer'
import MassesEauLayerSelection from './masses-eau-layer-selection'
import ViewContext from '@/contexts/view'

function SideMenu({children}) {
  const {view} = useContext(ViewContext)

  return (
    <div
      style={{minWidth: size.sideMenu, width: '550px'}}
      className='d-flex flex-column flex-grow-1'
    >
      <Container
        style={{overflow: 'hidden auto'}}
        className='d-flex flex-column flex-grow-1 p-2 bg-light'
      >
        {children}
      </Container>

      {view.key === 'infos' && (
        <Container className='mb-5 p-2 bg-light'>
          <MassesEauLayerSelection />
        </Container>
      )}

      <Container className='p-2'>
        <MenuFooter />
      </Container>
    </div>
  )
}

SideMenu.propTypes = {
  children: PropTypes.node.isRequired
}

export default SideMenu
