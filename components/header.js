import React, {useContext} from 'react'
import {Container, Breadcrumb, Dropdown, DropdownButton, Navbar, Nav} from 'react-bootstrap'

import size from '@/styles/size'

import ViewContext from '@/contexts/view'
import MapContext from '@/contexts/map'

const SESSION_ON = false

function Header() {
  const {views, bassinViews, view, sessions, selectedSession, setSelectedSession, handleSelectView} = useContext(ViewContext)
  const {selectedBassin} = useContext(MapContext)

  return (
    <Navbar style={{height: '64px'}} bg='light' expand='lg'>
      <div style={{minWidth: `calc(${size.sideMenu} - 2rem)`}} className='d-flex align-items-center'>
        <Breadcrumb className='w-100 mr-2'>
          {selectedBassin ? (
            <>
              <Breadcrumb.Item href='/'>Atlas DCE littoral</Breadcrumb.Item>
              <Breadcrumb.Item active>{selectedBassin.nom}</Breadcrumb.Item>
            </>
          ) : (
            <Breadcrumb.Item active>Atlas DCE littoral - France entière</Breadcrumb.Item>
          )}
        </Breadcrumb>

        {SESSION_ON && (
          <DropdownButton title={selectedSession} variant='secondary' menuAlign='right'>
            {sessions.map(session => (
              <Dropdown.Item
                key={session}
                eventKey={session}
                disabled={session === '2009-2014' || session === '2011-2016' || session === '2010-2015'}
                active={selectedSession === session}
                onSelect={setSelectedSession}
              >
                {session}
              </Dropdown.Item>
            ))}
          </DropdownButton>
        )}
      </div>

      <Container className='ml-3' fluid>
        <Nav variant='pills' activeKey={view.key} onSelect={handleSelectView}>
          {selectedBassin ? bassinViews.map(view => (
            <Nav.Item key={view.key}>
              <Nav.Link eventKey={view.key}>{view.name}</Nav.Link>
            </Nav.Item>
          )) : (
            views.map(view => (
              <Nav.Item key={view.key}>
                <Nav.Link eventKey={view.key}>{view.name}</Nav.Link>
              </Nav.Item>
            ))
          )}
        </Nav>
      </Container>

      <style global jsx>{`
          ol.breadcrumb {
            width: 100%;
            margin-bottom: 0;
          }

          .navbar-light .navbar-nav .active > .nav-link, .navbar-light .navbar-nav .nav-link.active, .navbar-light .navbar-nav .nav-link.show, .navbar-light .navbar-nav .show > .nav-link {
            color: #fff !important;
          }
        `}</style>
    </Navbar>
  )
}

export default Header
