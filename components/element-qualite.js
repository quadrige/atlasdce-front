import React, {useContext} from 'react'
import PropTypes from 'prop-types'
import {groupBy} from 'lodash'
import {Col, Container, Form, Nav, Row, Tab} from 'react-bootstrap'
import ElementsQualiteContext from '@/contexts/elements-qualite'

function ElementQualite({bassin}) {
  const {elementsQualiteCtx, setElementsQualiteCtx} = useContext(ElementsQualiteContext)
  const elementQualiteGroup = bassin ? groupBy(bassin.elementsQualite, 'typeElementQualiteNom') : {}

  return (
    <Container className='my-2'>
      <Form.Check
        custom
        className='mb-4 text-center font-weight-bold'
        selected
        checked={elementsQualiteCtx === 'none'}
        onChange={() => setElementsQualiteCtx('none')}
        type='radio'
        id='none'
        label='Aucun'
        value='none'
      />
      <Tab.Container id='left-tabs-example' defaultActiveKey='biologique'>
        <Row>
          <Col>
            <Nav variant='pills' className='flex-column'>
              {elementQualiteGroup['Etat biologique'] && (
                <Nav.Item>
                  <Nav.Link eventKey='biologique'>Etat biologique</Nav.Link>
                </Nav.Item>
              )}
              {elementQualiteGroup['Etat physico-chimique'] && (
                <Nav.Item>
                  <Nav.Link eventKey='physico-chimique'>Etat physico-chimique</Nav.Link>
                </Nav.Item>
              )}
              {elementQualiteGroup['Etat hydromorphologique'] && (
                <Nav.Item>
                  <Nav.Link eventKey='hydromorphologique'>Etat hydromorphologique</Nav.Link>
                </Nav.Item>
              )}
              {elementQualiteGroup['Etat chimique'] && (
                <Nav.Item>
                  <Nav.Link eventKey='chimique'>Etat chimique</Nav.Link>
                </Nav.Item>
              )}
            </Nav>
          </Col>
          <Col>
            <Tab.Content>
              {elementQualiteGroup['Etat biologique'] && (
                <Tab.Pane eventKey='biologique'>
                  {elementQualiteGroup['Etat biologique'].map(({id, nom, children}) => (
                    <div key={id}>
                      <div>
                        <Form.Check
                          custom
                          checked={elementsQualiteCtx === id}
                          onChange={() => setElementsQualiteCtx(id)}
                          type='radio'
                          id={id}
                          label={nom}
                          value={nom}
                        />
                      </div>
                      {children && children.map(({id, nom}) => (
                        <div key={id}>
                          <Form.Check
                            custom
                            className='ml-3'
                            checked={elementsQualiteCtx === id}
                            onChange={() => setElementsQualiteCtx(id)}
                            type='radio'
                            id={id}
                            label={nom}
                            value={nom}
                          />
                        </div>
                      ))}
                    </div>
                  ))}
                </Tab.Pane>
              )}
              {elementQualiteGroup['Etat physico-chimique'] && (
                <Tab.Pane eventKey='physico-chimique'>
                  {elementQualiteGroup['Etat physico-chimique'].map(({id, nom, children}) => (
                    <div key={id}>
                      <div>
                        <Form.Check
                          custom
                          checked={elementsQualiteCtx === id}
                          onChange={() => setElementsQualiteCtx(id)}
                          type='radio'
                          id={id}
                          label={nom}
                          value={nom}
                        />
                      </div>
                      {children && children.map(({id, nom}) => (
                        <div key={id}>
                          <Form.Check
                            custom
                            className='ml-3'
                            checked={elementsQualiteCtx === id}
                            onChange={() => setElementsQualiteCtx(id)}
                            type='radio'
                            id={id}
                            label={nom}
                            value={nom}
                          />
                        </div>
                      ))}
                    </div>
                  ))}
                </Tab.Pane>
              )}
              {elementQualiteGroup['Etat hydromorphologique'] && (
                <Tab.Pane eventKey='hydromorphologique'>
                  {elementQualiteGroup['Etat hydromorphologique'].map(({id, nom, children}) => (
                    <div key={id}>
                      <div>
                        <Form.Check
                          custom
                          checked={elementsQualiteCtx === id}
                          onChange={() => setElementsQualiteCtx(id)}
                          type='radio'
                          id={id}
                          label={nom}
                          value={nom}
                        />
                      </div>
                      {children && children.map(({id, nom}) => (
                        <div key={id}>
                          <Form.Check
                            custom
                            className='ml-3'
                            checked={elementsQualiteCtx === id}
                            onChange={() => setElementsQualiteCtx(id)}
                            type='radio'
                            id={id}
                            label={nom}
                            value={nom}
                          />
                        </div>
                      ))}
                    </div>
                  ))}
                </Tab.Pane>
              )}
              {elementQualiteGroup['Etat chimique'] && (
                <Tab.Pane eventKey='chimique'>
                  {elementQualiteGroup['Etat chimique'].map(({id, nom, children}) => (
                    <div key={id}>
                      <div>
                        <Form.Check
                          custom
                          checked={elementsQualiteCtx === id}
                          onChange={() => setElementsQualiteCtx(id)}
                          type='radio'
                          id={id}
                          label={nom}
                          value={nom}
                        />
                      </div>
                      {children && children.map(({id, nom}) => (
                        <div key={id}>
                          <Form.Check
                            custom
                            className='ml-3'
                            checked={elementsQualiteCtx === id}
                            onChange={() => setElementsQualiteCtx(id)}
                            type='radio'
                            id={id}
                            label={nom}
                            value={nom}
                          />
                        </div>
                      ))}
                    </div>
                  ))}
                </Tab.Pane>
              )}
            </Tab.Content>
          </Col>
        </Row>
      </Tab.Container>
    </Container>
  )
}

ElementQualite.propTypes = {
  bassin: PropTypes.object.isRequired
}

export default ElementQualite
