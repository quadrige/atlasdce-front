import React, {useContext} from 'react'
import PropTypes from 'prop-types'
import {useRouter} from 'next/router'
import {Table} from 'react-bootstrap'
import MapContext from '@/contexts/map'
import Link from 'next/link'

function MassesEauTable({isMapRendering, title, codeBassin, massesEauList}) {
  const {
    setSelectedPoint,
    setSelectedMasse,
    selectedMasse
  } = useContext(MapContext)
  const router = useRouter()

  const handleClick = (e, codeMasseEau) => {
    e.preventDefault()
    const masse = massesEauList.find(({code}) => code === codeMasseEau)

    setSelectedMasse(masse)
    setSelectedPoint(null)
    router.push(
      `/?map&bassin&codeBassin=${codeBassin}&masse&codeMasseEau=${masse.code}`,
      `/map/bassin/${codeBassin}/masse/${masse.code}`)
  }

  return (
    <div className='mt-3'>
      <h5 className='text-center'>
        {title}
      </h5>
      <Table responsive striped bordered size='sm'>
        <thead className='align-middle thead-dark'>
          <tr className='align-middle'>
            <th rowSpan='2' colSpan='2' className='text-center'>Identification de la masse d’eau</th>
            <th rowSpan='2' className='text-center'>Type</th>
            <th colSpan='3' className='text-center'>État provisoire</th>
          </tr>
          <tr>
            <td className='text-center'>chimique</td>
            <td className='text-center'>écologique</td>
            <td className='text-center'>Global</td>
          </tr>
        </thead>
        <tbody>
          {massesEauList && massesEauList.map(r => (
            <tr key={r.id}>
              <td>{r.code}</td>
              <td>
                {isMapRendering ? (
                  <a style={{color: `${r.code === selectedMasse?.code ? 'green' : ''}`}} href={`/map/bassin/${codeBassin}/masse/${r.code}`} onClick={e => handleClick(e, r.code)}>{r.nom}</a>
                ) : (
                  <Link
                    href={{
                      pathname: '/fiche/bassin/[bassin]/masse/[masse]',
                      query: {
                        bassin: codeBassin,
                        masse: r.code
                      }
                    }}
                  >
                    <a style={{color: `${r.code === selectedMasse?.code ? 'green' : ''}`}}>{r.nom}</a>
                  </Link>
                )}
              </td>
              <td className='text-center'>{r.type}</td>
              <td style={{backgroundColor: r.etat.chimique.couleur, width: 1}} />
              <td style={{backgroundColor: r.etat.ecologique.couleur, width: 1}} />
              <td style={{backgroundColor: r.etat.global.couleur, width: 1}} />
            </tr>
          ))}
        </tbody>

        <style jsx>{`
        thead > tr > th, tbody > tr > td {
            vertical-align: middle;
          }

        th, td {
          font-size: ${isMapRendering ? '70%' : '100%'};
        }
      `}</style>
      </Table>
    </div>
  )
}

MassesEauTable.propTypes = {
  isMapRendering: PropTypes.bool,
  title: PropTypes.string,
  codeBassin: PropTypes.string.isRequired,
  massesEauList: PropTypes.array.isRequired
}

MassesEauTable.defaultProps = {
  isMapRendering: true,
  title: null
}

export default MassesEauTable
