import React from 'react'
import PropTypes from 'prop-types'
import {Container} from 'react-bootstrap'

function Bassin({bassin}) {
  return (
    <Container>
      <p className='text-justify' dangerouslySetInnerHTML={{__html: bassin.alert}} /> {/* eslint-disable-line react/no-danger */}
    </Container>

  )
}

Bassin.propTypes = {
  bassin: PropTypes.object.isRequired
}

export default Bassin
