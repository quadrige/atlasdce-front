import React from 'react'
import PropTypes from 'prop-types'
import {Modal, Button, Table} from 'react-bootstrap'
import Link from 'next/link'

function ListReseauxModal(props) {
  return (
    <div>
      <Modal
        {...props}
        aria-labelledby='contained-modal-title-vcenter'
        centered
      >
        <Modal.Header closeButton />
        <Modal.Body>
          <Table responsive striped bordered size='sm'>
            <thead className='align-middle thead-dark'>
              <tr className='align-middle'>
                <th colSpan='2' className='text-center align-middle'>Identification du réseau</th>
              </tr>
              <tr>
                <td className='font-weight-bold text-center align-middle'>Code</td>
                <td className='font-weight-bold text-center align-middle'>Nom</td>
              </tr>
            </thead>
            <tbody>
              {props.reseaux.map(({id, code, nom}) => (
                <tr key={id}>
                  <td className='text-center font-weight-bold align-middle'>{code}</td>
                  <td className='text-center font-weight-bold align-middle'>
                    <Link href={{
                      pathname: '/fiche/bassin/[bassin]/reseau/[reseau]',
                      query: {
                        bassin: props.bassin,
                        reseau: code
                      }
                    }}
                    >
                      <a>{nom}</a>
                    </Link>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        </Modal.Body>
        <Modal.Footer>
          <Button onClick={props.onHide}>Fermer</Button>
        </Modal.Footer>
      </Modal>
    </div>
  )
}

ListReseauxModal.propTypes = {
  reseaux: PropTypes.array.isRequired,
  bassin: PropTypes.string.isRequired,
  onHide: PropTypes.func.isRequired
}

export default ListReseauxModal
