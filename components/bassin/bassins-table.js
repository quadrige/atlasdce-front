import React, {useState} from 'react'
import PropTypes from 'prop-types'
import {Table} from 'react-bootstrap'
import Link from 'next/link'
import ListReseauxModal from './list-reseaux-modal'

function BassinsTable({title, bassinsList, reseaux, stats}) {
  const [openModal, setOpenModal] = useState(false)
  const [selectedReseau, setSelectedReseau] = useState(null)
  const [selectedCodeBassin, setSelectedCodeBassin] = useState(null)

  const handleClick = bassinCode => {
    const reseau = reseaux.find(r => r.bassinCode === bassinCode)
    setSelectedCodeBassin(bassinCode)
    setSelectedReseau(reseau.reseaux)
    setOpenModal(true)
  }

  return (
    <div className='mt-3'>
      {title && (
        <h5 className='text-center'>
          {title}
        </h5>
      )}
      <Table responsive striped bordered size='sm'>
        <thead className='align-middle thead-dark'>
          <tr className='align-middle'>
            <th rowSpan='2' colSpan='2' className='text-center align-middle'>Identification du bassin</th>
            <th rowSpan='2' className='text-center align-middle'>Description</th>
            <th colSpan='3' className='text-center align-middle'>Statistiques</th>
          </tr>
          <tr>
            <td className='text-center align-middle'>masses d’eau cotières</td>
            <td className='text-center align-middle'>masses d’eau de transitions</td>
            <td className='text-center align-middle'>points de surveillance</td>
          </tr>
        </thead>
        <tbody>
          {bassinsList && bassinsList.map(b => (
            <tr key={b.id}>
              <td className='text-center font-weight-bold align-middle'>{b.code}</td>
              <td className='text-center align-middle'>
                <Link href={{
                  pathname: '/fiche/bassin/[bassin]',
                  query: {
                    bassin: b.code
                  }
                }}
                ><a>{b.nom}</a></Link>
                <br />
                <hr />
                <a href='#' onClick={() => handleClick(b.code)}>Accéder aux réseaux</a>
              </td>
              <td className='text-center align-middle' dangerouslySetInnerHTML={{__html: b.alert}} /> {/* eslint-disable-line react/no-danger */}
              <td className='text-center font-weight-bold align-middle'>{stats.find(({code}) => code === b.code).massesCotieres}</td>
              <td className='text-center font-weight-bold align-middle'>{stats.find(({code}) => code === b.code).massesTransitions}</td>
              <td className='text-center font-weight-bold align-middle'>{stats.find(({code}) => code === b.code).points}</td>
            </tr>
          ))}
        </tbody>
      </Table>
      {selectedReseau && (
        <ListReseauxModal
          reseaux={selectedReseau}
          bassin={selectedCodeBassin}
          show={openModal}
          onHide={() => setOpenModal(false)}
        />
      )}
    </div>
  )
}

BassinsTable.propTypes = {
  title: PropTypes.string,
  reseaux: PropTypes.array.isRequired,
  bassinsList: PropTypes.array.isRequired,
  stats: PropTypes.array.isRequired
}

BassinsTable.defaultProps = {
  title: null
}

export default BassinsTable
