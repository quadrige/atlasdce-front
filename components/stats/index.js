import React from 'react'
import PropTypes from 'prop-types'

import Counter from '@/components/stats/counter'
import {Container, Row, Col} from 'react-bootstrap'

const STATS_LABELS = {
  bassinsMetropole: 'bassins en métropole',
  bassinsOutremers: 'bassins outre-mer',
  massesCotieres: 'masses d’eau cotières',
  massesTransitions: 'masses d’eau de transitions',
  points: 'points de surveillance',
  parametres: 'paramètres'
}

function Stats({stats}) {
  return (
    <Container fluid>
      <Row>
        {Object.keys(stats).map(key => (
          <React.Fragment key={key}>
            {stats[key] > 0 && (
              <Col>
                <Counter value={stats[key]} label={STATS_LABELS[key]} />
              </Col>
            )}
          </React.Fragment>
        ))}
      </Row>
    </Container>
  )
}

Stats.propTypes = {
  stats: PropTypes.object.isRequired
}

export default Stats
