import React from 'react'
import PropTypes from 'prop-types'

function Counter({value, label, color, hexaColor}) {
  return (
    <div className={`d-flex flex-column align-items-center m-2 text-${color}`}>
      <div style={{fontSize: '1.5rem', color: hexaColor}} className='font-weight-bold'>{value}</div>
      <div style={{fontSize: '1rem', color: hexaColor}} className='text-center'>{label}</div>
    </div>
  )
}

Counter.propTypes = {
  value: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number
  ]).isRequired,
  label: PropTypes.string,
  color: PropTypes.oneOf([
    'success',
    'warning',
    'danger',
    'info',
    'dark'
  ]),
  hexaColor: PropTypes.string
}

Counter.defaultProps = {
  label: null,
  color: 'dark',
  hexaColor: '#000000'
}

export default Counter
