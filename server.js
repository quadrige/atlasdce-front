const express = require('express')
const compression = require('compression')
const next = require('next')

const port = process.env.PORT || 3000
const dev = process.env.NODE_ENV !== 'production'
const app = next({dev})
const handle = app.getRequestHandler()

app.prepare().then(() => {
  const server = express()

  if (!dev) {
    server.use(compression())
  }

  server.use(express.static('public'))

  server.all('/_next/webpack-hmr', (req, res) => {
    return handle(req, res)
  })

  server.get('/', (request, response) => {
    response.redirect('/map')
  })

  server.get('/map', (req, res) => {
    app.render(req, res, '/', {
      ...req.query
    })
  })

  server.get('/map/bassin/:codeBassin', (req, res) => {
    app.render(req, res, '/', {
      ...req.query,
      codeBassin: req.params.codeBassin
    })
  })

  server.get('/map/bassin/:codeBassin/masse/:codeMasseEau', (req, res) => {
    app.render(req, res, '/', {
      ...req.query,
      codeBassin: req.params.codeBassin,
      codeMasseEau: req.params.codeMasseEau
    })
  })

  server.get('/fiche', (req, res) => {
    app.render(req, res, '/fiche', {
      ...req.query
    })
  })

  server.get('/fiche/bassin/:bassin', (req, res) => {
    app.render(req, res, '/fiche/bassin/[bassin]', {
      ...req.query,
      bassin: req.params.bassin
    })
  })

  server.get('/fiche/bassin/:bassin/masse/:masse', (req, res) => {
    app.render(req, res, '/fiche/bassin/[bassin]/masse/[masse]', {
      ...req.query,
      bassin: req.params.bassin,
      masse: req.params.masse
    })
  })

  server.get('/fiche/bassin/:bassin/reseau/:reseau', (req, res) => {
    app.render(req, res, '/fiche/bassin/[bassin]/reseau/[reseau]', {
      ...req.query,
      bassin: req.params.bassin,
      reseau: req.params.reseau
    })
  })

  server.get('*', (req, res) => {
    return handle(req, res)
  })

  server.listen(port, error => {
    if (error) {
      throw error
    }
  })

  console.log(`> Ready on http://127.0.0.1:${port}`)
})
