import {format} from 'date-fns'
import {fr} from 'date-fns/locale'

export function pointsToFeatureCollections(points) {
  return {
    type: 'FeatureCollection',
    features: points.map(point => {
      const {lon, lat, ...properties} = point
      return {
        type: 'Feature',
        properties,
        geometry: {
          type: 'Point',
          coordinates: [lon, lat]
        }
      }
    })
  }
}

export function bassinsToFeatureCollections(bassins, bassinsFromDatabase) {
  return {
    type: 'FeatureCollection',
    features: bassins.map(bassin => {
      const {lon, lat, properties} = bassin
      const {id, code, nom} = properties
      const {dateMaj} = bassinsFromDatabase.find(b => b.id === id)

      return {
        type: 'Feature',
        properties: {
          id,
          code,
          nom,
          dateMaj: `(MAJ : ${format(new Date(dateMaj.date), 'P', {locale: fr})})`
        },
        geometry: {
          type: 'Point',
          coordinates: [lon, lat]
        }
      }
    })
  }
}
