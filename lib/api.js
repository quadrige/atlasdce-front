const ATLAS_API_URL = process.env.DOCKER_ATLAS_API_URL || process.env.NEXT_PUBLIC_ATLAS_API_URL

export async function _fetch(url) {
  const options = {
    mode: 'cors',
    method: 'GET'
  }

  const response = await fetch(url, options)
  const contentType = response.headers.get('content-type')

  if (!response.ok) {
    throw new Error(response.statusMessage)
  }

  if (response.ok && contentType && contentType.includes('application/json')) {
    return response.json()
  }

  throw new Error('Une erreur est survenue')
}

export function getStats() {
  return _fetch(`${ATLAS_API_URL}/stats`)
}

export function getBassinStats(bassinCode) {
  return _fetch(`${ATLAS_API_URL}/stats/${bassinCode}`)
}

export function getMassesEau() {
  return _fetch(`${ATLAS_API_URL}/masses-eau`)
}

export function getMasseEau(id) {
  return _fetch(`${ATLAS_API_URL}/masses-eau/${id}`)
}

export function getMasseEauByCode(masseCode) {
  return _fetch(`${ATLAS_API_URL}/masses-eau/code/${masseCode}`)
}

export function getMassesEauByBassinId(bassinId) {
  return _fetch(`${ATLAS_API_URL}/masses-eau/bassin/${bassinId}`)
}

export function getBassins() {
  return _fetch(`${ATLAS_API_URL}/bassins`)
}

export function getBassin(id) {
  return _fetch(`${ATLAS_API_URL}/bassins/${id}`)
}

export function getBassinByCode(code) {
  return _fetch(`${ATLAS_API_URL}/bassins/code/${code}`)
}

export function getPoints() {
  return _fetch(`${ATLAS_API_URL}/points`)
}

export function getPoint(id) {
  return _fetch(`${ATLAS_API_URL}/points/${id}`)
}

export function getPointByCode(code) {
  return _fetch(`${ATLAS_API_URL}/points/code/${code}`)
}

export function getReseaux() {
  return _fetch(`${ATLAS_API_URL}/reseaux/`)
}

export function getReseauxByBassinId(bassinId) {
  return _fetch(`${ATLAS_API_URL}/reseaux/bassin/${bassinId}`)
}

export function getReseau(id) {
  return _fetch(`${ATLAS_API_URL}/reseaux/${id}`)
}

export function getReseauByCode(code, bassinId) {
  return _fetch(`${ATLAS_API_URL}/reseaux/${code}/bassin/${bassinId}`)
}

export function getParametres() {
  return _fetch(`${ATLAS_API_URL}/parametres}`)
}

export function getParametre(id) {
  return _fetch(`${ATLAS_API_URL}/parametres/${id}`)
}

export function getReseauPointParam(pointId) {
  return _fetch(`${ATLAS_API_URL}/reseau-point-param/${pointId}`)
}
