export const formatLaReunionCode = code => {
  const startCode = code.slice(0, 4)

  if (startCode === 'FRLC') {
    const endCode = code.slice(-2)
    return `${startCode}1${endCode}`
  }

  return code
}
