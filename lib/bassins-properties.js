export const bassinsProperties = [
  {
    properties: {
      id: 1,
      code: 'LB',
      CdEuBassin: 'FRG',
      nom: 'Loire-Bretagne'
    },
    lon: 0.74749,
    lat: 47.23489
  },
  {
    properties: {
      id: 2,
      code: 'AG',
      CdEuBassin: 'FRF',
      nom: 'Adour-Garonne'
    },
    lon: 1.008574,
    lat: 44.404253
  },
  {
    properties: {
      id: 3,
      code: 'SN',
      CdEuBassin: 'FRH',
      nom: 'Seine Normandie'
    },
    lon: 2.5931,
    lat: 49.08151
  },
  {
    properties: {
      id: 4,
      code: 'AP',
      CdEuBassin: 'FRA',
      nom: 'Artois Picardie'
    },
    lon: 2.83251,
    lat: 50.31669
  },
  {
    properties: {
      id: 5,
      code: 'RMC',
      CdEuBassin: 'FRD',
      nom: 'Rhône et côtiers méditerranéens'
    },
    lon: 5.8452,
    lat: 44.88672
  },
  {
    properties: {
      id: 6,
      code: 'RMCC',
      CdEuBassin: 'FRE',
      nom: 'Corse'
    },
    lon: 9.049755,
    lat: 42.188176
  }
]
