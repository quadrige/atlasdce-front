# Pré-requis

- [Node.js](https://nodejs.org/) >=14 <18
- [Yarn](https://www.yarnpkg.com/)
- [atlasdce-api](https://gitlab.ifremer.fr/quadrige/atlasdce)

## Variables d'environnement (fichier .env utilisé par défaut)


Afin de communiquer avec la partie backend, il est nécessaire de renseigner l'adresse de l'API.
En cas de déploiement via Docker, les 2 variables suivantes sont requises:

- `DOCKER_ATLAS_API_URL` (appels côté serveur: par défaut *http://atlasdce-api*)
- `NEXT_PUBLIC_ATLAS_API_URL` (appels côté client: par défaut *http://127.0.0.1:8000*)

En cas de développement ou de déploiement en dehors de docker `DOCKER_ATLAS_API_URL` doit être vide.

`NEXT_PUBLIC_ATLAS_API_URL` fait référence à l'adresse de l'API.
Sa valeur dépend donc du déploiement de la partie backend, [atlasdce-api](https://gitlab.ifremer.fr/quadrige/atlasdce) (domaine ou adresse IP).

## Intégration continue

Le processus d'intégration continue permet de générer l'image docker à partir des sources SVN du projet.

| Etape | Tâches | Description | Executeur |
|-|-|-|-|
| package | docker:develop | Création d'une image docker taggé **develop** permettant d'éxecuter l'application | push sur branche master |
| package | docker:tag | Création d'une image docker taggé **CI_COMMIT_TAG** permettant d'éxecuter l'application | création d'un tag |


# Développement

### Installation
```
yarn
``` 

### Lancement du server
```
yarn dev
```

# Docker

## Démarrer le front

### En buildant l'image en local

Localement avec docker composer et le fichier environnement pas defaut (`.env`)

```bash
docker-compose up 
```

Ou en utilisant le script d'execution :

```bash
docker compose build
./docker-run.sh
```

/!\ à bien déposer un dump de la base de données dans le répertoire `./database/` du projet pour que la base de données soit initialisée.

### En récupérant l'image sur la registry

Commande à exécuter depuis une machine unix avec docker installé :

```bash
./docker-run.sh .env.registry
```