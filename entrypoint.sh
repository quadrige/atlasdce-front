#!/bin/sh

find /atlasdce-front/.next \( -type d -name .git -prune \) -o -type f -print0 | xargs -0 sed -i "s#APP_NEXT_PUBLIC_ATLAS_API_URL#$NEXT_PUBLIC_ATLAS_API_URL#g"

find /atlasdce-front/.next \( -type d -name .git -prune \) -o -type f -print0 | xargs -0 sed -i "s#APP_NEXT_PUBLIC_DOWNLOAD_PATH#$NEXT_PUBLIC_DOWNLOAD_PATH#g"

echo "Starting Nextjs"
exec "$@"
