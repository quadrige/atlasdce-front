import React, {useContext} from 'react'
import PropTypes from 'prop-types'

import size from '@/styles/size'

import Header from '@/components/header'
import SideMenu from '@/components/side-menu'
import MapWrapper from '@/components/map/map-wrapper'
import ViewContext from '@/contexts/view'

function MainLayout({children}) {
  const {isMenuShown} = useContext(ViewContext)

  return (
    <div className='d-flex flex-column flex-grow-1'>
      <Header />
      <div style={{height: `calc(100% - ${size.header})`}} className='d-flex'>
        {isMenuShown && (
          <SideMenu>
            {children}
          </SideMenu>
        )}
        <MapWrapper />
      </div>
    </div>
  )
}

MainLayout.propTypes = {
  children: PropTypes.node.isRequired
}

export default MainLayout
